/**
*	Esta clase es un beans de Usuario.
*	@author Hermes Sotoj
*/
package org.hermes.beans;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;

public class Usuario{
	private IntegerProperty idUsuario, idModulo, edad;
	private StringProperty nombre, pass, nick, nombreModulo;
	/**
	*	Este es el constructor de la clase se ejecuta cada vez que se instancia la clase.
	*/
	public Usuario(){
		nombreModulo = new SimpleStringProperty();
		idUsuario = new SimpleIntegerProperty();
		idModulo = new SimpleIntegerProperty();
		edad = new SimpleIntegerProperty();
		nombre = new SimpleStringProperty();
		pass = new SimpleStringProperty();
		nick= new SimpleStringProperty();
	}
	/**
	*	Este es el constructor de la clase se ejecuta cada vez que se instancia la clase.
	*	@param idUsuario Esta variable se encarga de almacenar el id del Usuario
	*	@param idModulo Esta variable se encarga de almacenar el id del modulo al que pertenece este usuario
	*	@param nombre Esta variable se encarga de almacenar el nombre del usuario
	*	@param pass Esta variable se encarga de almacenar la contraseña del usuario
	*	@param edad Esta variable se encarga de almacenar la edad del usuario
	*	@param nick Esta variable se encarga de almacenar el nick del usuario
	*	@param nombreModulo Esta variable se encarga de almacenar el
	*/
	public Usuario(int idUsuario, int idModulo, String nombre, String pass, int edad, String nick, String nombreModulo){
		this.idUsuario = new SimpleIntegerProperty(idUsuario);
		this.nombreModulo = new SimpleStringProperty(nombreModulo);
		this.idModulo = new SimpleIntegerProperty(idModulo);
		this.nick = new SimpleStringProperty(nick);
		this.edad = new SimpleIntegerProperty(edad);
		this.nombre = new SimpleStringProperty(nombre);
		this.pass = new SimpleStringProperty(pass);
	}
	/**
	*	Este metodo nos devuelve el id de un usuario
	*	@return idUsuario Contiene el valor del id de un Usuario
	*/
	public int getIdUsuario(){
		return idUsuario.get();
	}
	/**
	*	Este metodo nos sirve para almacenar el valor del id del Usuario
	*	@param idUsuario Esta variable guarda el valor del id de un Usuario
	*/
	public void setIdUsuario(int idUsuario){
		this.idUsuario.set(idUsuario);
	}
	/**
	*	Este metodo nos devuelve el id de un usuario
	*	@return idUsuario Contiene el valor del id de un Usuario
	*/
	public IntegerProperty idUsuarioProperty(){
		return idUsuario;
	}
	/**
	*	Este metodo nos devuelve el id del Modulo al que el usuario pertenece
	*	@return idModulo Nos devuelve el id del modulo al que el usuario pertenece
	*/
	public int getIdModulo(){
		return idModulo.get();
	}
	/**
	*	Este metodo nos sirve para poder guardar el id del modulo al que el usuario pertenece
	*	@param idModulo Es la variable que contiene el id del modulo que se va a guardar
	*/
	public void setIdModulo(int idModulo){
		this.idModulo.set(idModulo);
	}
	/**
	*	Este metodo nos devuelve el id del Modulo al que el usuario pertenece
	*	@return idModulo Nos devuelve el id del modulo al que el usuario pertenece
	*/
	public IntegerProperty idModuloProperty(){
		return idModulo;
	}
	/**
	*	Este metodo nos devuelve la edad del usuario
	*	@return edad Es la varaible que contiene el valor de la edad del usuario
	*/
	
	public int getEdad(){
		return edad.get();
	}
	/**
	*	Este metodo sirve para que se pueda guardar la edad del usuario.
	*	@param edad Es la variable a almacenar con el valor de la edad.
	*/
	public void setEdad(int edad){
		this.edad.set(edad);
	}
	/**
	*	Este metodo nos devuelve la edad del usuario
	*	@return edad Es la varaible que contiene el valor de la edad del usuario
	*/
	public IntegerProperty EdadProperty(){
		return edad;
	}
	/**
	*	Este metodo es el que devuelve el nombre del Usuario
	*	@return nombre Esta variable es la que se devuelve la cual contiene el nombre del usuario
	*/

	public String getNombre(){
		return nombre.get();
	}
	/**
	*	Este metodo es el que almacena el nombre del usuario
	*	@param nombre Es la variable que contiene el valor del nombre del usuario.
	*/
	public void setNombre(String nombre){
		this.nombre.set(nombre);
	}
	/**
	*	Este metodo es el que devuelve el nombre del Usuario
	*	@return nombre Esta variable es la que se devuelve la cual contiene el nombre del usuario
	*/
	public StringProperty nombreProperty(){
		return nombre;
	}
	/**
	*	Este metodo nos devuelve el nombre del Modulo al que el usuario pertenece
	*	@return nombreModulo Es el nombre del modulo al que el usuario pertenece
	*/
	
	public String getModulo(){
		return nombreModulo.get();
	}
	/**
	*	Este metodo sirve para poder almacenar el nombre del modulo al que el usuario pertenece
	*	@param nombreModulo Esta variable es la que almacena el valor del nombre del modulo.
	*/
	public void setModulo(String nombreModulo){
		this.nombreModulo.set(nombreModulo);
	}
	/**
	*	Este metodo nos devuelve el nombre del Modulo al que el usuario pertenece
	*	@return nombreModulo Es el nombre del modulo al que el usuario pertenece
	*/
	public StringProperty moduloProperty(){
		return nombreModulo;
	}
	/**
	*	Este metodo nos devuelve el nick de un usuario.
	*	@return nick Esta variable nos devuelve el nick de un usuario
	*/
	public String getNick(){
		return nick.get();
	}
	/**
	*	Este metodo nos sirve para almacenar el nick de un usuario.
	*	@param nick Esta variable es la encargada de almacenar el nick del usuario.
	*/
	public void setNick(String nick){
		this.nick.set(nick);
	}
	/**
	*	Este metodo nos devuelve el nick de un usuario.
	*	@return nick Esta variable nos devuelve el nick de un usuario
	*/
	public StringProperty nickProperty(){
		return nick;
	}
	
	/**
	*	Este metodo es el que devuelve la contrasña del usuario
	*	@return pass Esta variable contiene el valor de la contraseña de un usario.
	*/
	public String getPass(){
		return pass.get();
	}
	/**
	*	Este metodo es el que almacena la contraseña de un usuario.
	*	@param pass Esta variable es la que contiene el valor de la contraseña del usuario.
	*/
	public void setPass(String pass){
		this.pass.set(pass);
	}
	/**
	*	Este metodo es el que devuelve la contrasña del usuario
	*	@return pass Esta variable contiene el valor de la contraseña de un usario.
	*/
	public StringProperty passProperty(){
		return pass;
	}
}
