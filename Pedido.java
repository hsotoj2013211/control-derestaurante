/**
*	Esta clase es un beans de Usuario.
*	@author Hermes Sotoj
*/
package org.hermes.beans;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;

public class Pedido{
	private IntegerProperty idPedido,totalPedido,idOrden;
	private StringProperty nombre;
	/**
	*	Este es el constructor de la clase se ejecuta cada vez que se instancia la clase.
	*/
	public Pedido(){
		idPedido = new SimpleIntegerProperty();
		idOrden = new SimpleIntegerProperty();
		nombre = new SimpleStringProperty();
		totalPedido = new SimpleIntegerProperty();
	}
	/**
	*	Este es el constructor de la clase se ejecuta cada vez que se instancia la clase.
	*	@param idUsuario Esta variable se encarga de almacenar el id del Usuario
	*	@param idModulo Esta variable se encarga de almacenar el id del modulo al que pertenece este usuario
	*	@param nombre Esta variable se encarga de almacenar el nombre del usuario
	*	@param pass Esta variable se encarga de almacenar la contraseña del usuario
	*	@param edad Esta variable se encarga de almacenar la edad del usuario
	*	@param nick Esta variable se encarga de almacenar el nick del usuario
	*	@param nombreModulo Esta variable se encarga de almacenar el
	*/
	public Pedido(int idPedido,String nombre,int totalPedido,int idOrden){
		this.idPedido = new SimpleIntegerProperty(idPedido);
		this.idOrden = new SimpleIntegerProperty(idOrden);
		this.nombre = new SimpleStringProperty(nombre);
		this.totalPedido = new SimpleIntegerProperty(totalPedido);
	}
	/**
	*	Este metodo nos devuelve el id de un usuario
	*	@return idUsuario Contiene el valor del id de un Usuario
	*/
	public int getIdPedido(){
		return idPedido.get();
	}
	/**
	*	Este metodo nos sirve para almacenar el valor del id del Usuario
	*	@param idUsuario Esta variable guarda el valor del id de un Usuario
	*/
	public void setIdPedido(int idPedido){
		this.idPedido.set(idPedido);
	}
	/**
	*	Este metodo nos devuelve el id de un usuario
	*	@return idUsuario Contiene el valor del id de un Usuario
	*/
	public IntegerProperty idPedidoProperty(){
		return idPedido;
	}

	public String getNombre(){
		return nombre.get();
	}
	/**
	*	Este metodo es el que almacena el nombre del usuario
	*	@param nombre Es la variable que contiene el valor del nombre del usuario.
	*/
	public void setNombre(String nombre){
		this.nombre.set(nombre);
	}
	/**
	*	Este metodo es el que devuelve el nombre del Usuario
	*	@return nombre Esta variable es la que se devuelve la cual contiene el nombre del usuario
	*/
	public StringProperty nombreProperty(){
		return nombre;
	}
	public int getTotal(){
		return totalPedido.get();
	}
	public void setTotal(int totalPedido){
		this.totalPedido.set(totalPedido);
	}
	public IntegerProperty TotalProperty(){
		return totalPedido;
	}
	public int getIdOrden(){
		return idOrden.get();
	}
	/**
	*	Este metodo nos sirve para almacenar el valor del id del Usuario
	*	@param idUsuario Esta variable guarda el valor del id de un Usuario
	*/
	public void setIdOrden(int idOrden){
		this.idOrden.set(idOrden);
	}
	/**
	*	Este metodo nos devuelve el id de un usuario
	*	@return idUsuario Contiene el valor del id de un Usuario
	*/
	public IntegerProperty idOrdenProperty(){
		return idOrden;
	}
}
