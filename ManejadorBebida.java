/**
*	Esta clase es el manejador de Bebida para poder manipular todos los datos de las bebidas.
*/
package org.hermes.manejadores;

import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import org.hermes.db.Conexion;
import org.hermes.beans.Bebida;

public class ManejadorBebida{
	private Conexion cnx;
	private ObservableList<Bebida> listaDeBebidas;
	/**
	*	Este es el constructor de la clase ManejadorBebida se ejecuta cada vez que se instancia la clase.
	*	@param cnx Esta variable es la que se encarga de recivir la conexion a la base de datos.
	*/
	public ManejadorBebida(Conexion cnx){
		this.cnx=cnx;
		this.listaDeBebidas=FXCollections.observableArrayList();
	}
	/**
	*	Este metodo nos sirve para poder actualizar la lista cada vez que se hace un cambio en la base de datos.
	*/
	public void actualizarListaDeBebidas(){
		listaDeBebidas.clear();
		ResultSet resultado = cnx.ejecutarConsulta("SELECT idBebida, precio, nombre FROM Bebida");
		try{
			while(resultado.next()){
				Bebida Bebida = new Bebida(resultado.getInt("idBebida"), resultado.getInt("precio"), resultado.getString("nombre"));
				listaDeBebidas.add(Bebida);
			}
		}catch(SQLException sql){
			sql.printStackTrace();
		}
	}
	/**
	*	Este metodo nos sirve para poder actuarlizar la lista de Bebidas con los datos de la base de datos.
	*	@return listaDeBebidas Devuelve la lista de Bebidas ya actualizadas.
	*/
	public ObservableList<Bebida> getListaDeIngredientes(){
		actualizarListaDeBebidas();
		return listaDeBebidas;
	}
	/**
	*	Este metodo sirve para eliminar un Bebida.
	*	@param Bebida es el Bebida que en este caso se va a eliminar
	*/
	public void eliminarBebida(Bebida bebida){
		cnx.ejecutarSentencia("DELETE FROM Bebida WHERE idBebida="+bebida.getIdBebida());
		actualizarListaDeBebidas();
	}
	/**
	*	Este metodo sirve para agregar un Bebida.
	*	@param Bebida es el Bebida que en este caso se va a agregar
	*/
	public void agregarBebida(Bebida bebida){
		cnx.ejecutarSentencia("INSERT INTO Bebida(nombre,precio) VALUES ('"+bebida.getNombre()+"',"+bebida.getPrecio()+")");
		actualizarListaDeBebidas();
	}
	/**
	*	Este metodo sirve para modificar un Bebida.
	*	@param bebida es el bebida que en este caso se va a agregar
	*/
	public void modificarBebida(Bebida bebida){
		cnx.ejecutarSentencia("UPDATE Bebida SET nombre='"+bebida.getNombre()+"', precio="+bebida.getPrecio()+" WHERE idBebida="+bebida.getIdBebida());
		actualizarListaDeBebidas();
	}
}

