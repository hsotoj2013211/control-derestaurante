/**
*
*/
package org.hermes.manejadores;

import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import org.hermes.db.Conexion;
import org.hermes.beans.Pedido;

public class ManejadorPedido{
	private Conexion cnx;
	private ObservableList<Pedido> listaPedidos;
	/**
	*	Este es el constructor de la clase ManejadorIngrediente se ejecuta cada vez que se instancia la clase.
	*	@param cnx Esta variable es la que se encarga de recivir la conexion a la base de datos.
	*/
	public ManejadorPedido(Conexion cnx){
		this.cnx=cnx;
		this.listaPedidos=FXCollections.observableArrayList();
	}
	/**
	*	Este metodo nos sirve para poder actualizar la lista cada vez que se hace un cambio en la base de datos.
	*/
	public void actualizarListaDePedidos(){
		listaPedidos.clear();
		ResultSet resultado = cnx.ejecutarConsulta("SELECT idPedido, totalPedido,nombre,idOrden FROM Pedido");
		try{
			while(resultado.next()){
				Pedido ped = new Pedido(resultado.getInt("idPedido"), resultado.getString("nombre"), resultado.getInt("totalPedido"), resultado.getInt("idOrden"));
				listaPedidos.add(ped);
			}
		}catch(SQLException sql){
			sql.printStackTrace();
		}
	}
	/**
	*	Este metodo nos sirve para poder actuarlizar la lista de ingredientes con los datos de la base de datos.
	*	@return listaPedidos Devuelve la lista de ingredientes ya actualizadas.
	*/
	public ObservableList<Pedido> getListaPedidos(){
		actualizarListaDePedidos();
		return listaPedidos;
	}
	/**
	*	Este metodo sirve para eliminar un ingrediente.
	*	@param ingrediente es el ingrediente que en este caso se va a eliminar
	*/
	public void eliminarPedido(Pedido pedido){
		cnx.ejecutarSentencia("DELETE FROM Pedido WHERE idPedido="+pedido.getIdPedido());
		actualizarListaDePedidos();
	}
	/**
	*	Este metodo sirve para agregar un ingrediente.
	*	@param ingrediente es el ingrediente que en este caso se va a agregar
	*/
	public void agregarPedido(Pedido pedido){
		cnx.ejecutarSentencia("INSERT INTO Pedido(nombre,idOrden,totalPedido) VALUES ('"+pedido.getNombre()+"', "+pedido.getIdOrden()+", "+pedido.getTotal()+")");
		actualizarListaDePedidos();
	}
	/**
	*	Este metodo sirve para modificar un ingrediente.
	*	@param ingrediente es el ingrediente que en este caso se va a agregar
	*/
	public void modificarPedido(Pedido ped){
		cnx.ejecutarSentencia("UPDATE Pedido SET nombre='"+ped.getNombre()+", "+"totalPedido='"+ped.getTotal()+" "+" WHERE idOrden="+ped.getIdPedido());
		actualizarListaDePedidos();
	}
}

