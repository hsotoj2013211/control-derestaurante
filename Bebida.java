/**
*	Esta clase es el beans de Bebida aqui se guarda todo con respecto a un bebida.
*	@return Hermes Sotoj
*/
package org.hermes.beans;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;

public class Bebida{
	private IntegerProperty idBebida, costo;
	private StringProperty nombre;
	/**
	*	Este es el constructor se ejecuta cada ves que se instancia esta clase.
	*/
	public Bebida(){
		idBebida = new SimpleIntegerProperty();
		costo = new SimpleIntegerProperty();
		nombre = new SimpleStringProperty("");
	}
	/**
	*	Este es un constructor el cual pide parametros.
	*	@param idBebida Esta variable es la que almacena el id del Bebida.
	*   @param costo Esta variable es la que almacena el costo del bebida.
	*	@param nombre Esta variable es la que almacena el nombre del bebida
	.
	*/
	public Bebida(int idBebida, int costo, String nombre){
		this.idBebida = new SimpleIntegerProperty(idBebida);
		this.costo = new SimpleIntegerProperty(costo);
		this.nombre = new SimpleStringProperty(nombre);
	}
	/**
	*	Este metodo es el que devuelve el id de un Ingrediente.
	*	@return idBebida Devuelve el valor del id del ingrediente.
	*/
	public int getIdBebida(){
		return idBebida.get();
	}
	/**
	*	Este metodo es el que almacena el valor del id del Ingrediente.
	*	@param idIngrediente En esta variable se almacena el valor del id.
	*/
	public void setIdBebida(int idBebida){
		this.idBebida.set(idBebida);
	}
	/**
	*	Este metodo devuelve el id del ingrediente.
	*	@return idIngrediente Devuelve el id del ingrediente.
	*/
	public IntegerProperty idIngredienteProperty(){
		return idBebida;
	}
	/**
	*	Este metodo devuelve el precio del ingrediente como tipo entero.
	*	@return costo Devuelve el precio del ingrediente como tipo entero.
	*/
	public int getPrecio(){
		return costo.get();
	}
	/**
	*	Este metodo almacena el precio del ingrediente.
	*	@param costo Almacena el precio del ingrediente.
	*/
	public void setPrecio(int costo){
		this.costo.set(costo);
	}
	/**
	*	Este metodo devuelve el costo del ingrediente.
	*	@return costo Devuelve el costo del ingrediente.
	*/
	public IntegerProperty PrecioProperty(){
		return costo;
	}
	/**
	*	Este metodo devuelve el nombre del ingrediente como String.
	*	@return nombre Devuelve el nombre del ingrediente.
	*/
	public String getNombre(){
		return nombre.get();
	}
	/**
	*	Este metodo almacena el nombre del ingrediente.
	*	@param nombre Almacena el nombre del ingrediente.
	*/
	public void setNombre(String nombre){
		this.nombre.set(nombre);
	}
	/**
	*	Este metodo devuelve el valor del nombre de un ingrediente.
	* 	@return nombre Es el nombre del ingrediente.
	*/
	public StringProperty nombreProperty(){
		return nombre;
	}
}
