/**
*	Esta clase es el manejador de Platillo todo lo que se puede hacer con el platillo esta aqui.
*	@author Hermes Sotoj
*/
package org.hermes.manejadores;

import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import org.hermes.db.Conexion;
import org.hermes.beans.Platillo;

public class ManejadorPlatillo{
	private Conexion cnx;
	private ObservableList<Platillo> listaDePlatillos;
	/**
	*	Este es el constructor de la clase se ejecuta cada vez que se instancia la clase.
	*	@param cnx Es la variable encargada de generar la conexion con la base de datos.
	*/
	public ManejadorPlatillo(Conexion cnx){
		this.cnx=cnx;
		this.listaDePlatillos=FXCollections.observableArrayList();
	}
	/**
	*	Este metodo nos sirve para mantener actualizada los datos con los de la base de datos
	*/
	public void actualizarListaDePlatillos(){
		listaDePlatillos.clear();
		ResultSet resultado = cnx.ejecutarConsulta("SELECT idPlatillo, precio, nombre FROM Platillo");
		try{
			while(resultado.next()){
				Platillo platillo = new Platillo(resultado.getInt("idPlatillo"), resultado.getInt("precio"), resultado.getString("nombre"));
				listaDePlatillos.add(platillo);
			}
		}catch(SQLException sql){
			sql.printStackTrace();
		}
	}
	/**
	*	Este metodo nos sirve para mantener actualizada la lista de platillos con respecto de los de la base de datos.
	*	@return listaDePlatillos Devuelve la lista actualizada de platillos.
	*/
	public ObservableList<Platillo> getListaDePlatillos(){
		actualizarListaDePlatillos();
		return listaDePlatillos;
	}
	/**
	*	Este metodo nos sirve para poder eliminar un platillo.
	*	@param platillo Este es el platillo que en este caso se va a eliminar.
	*/
	public void eliminarPlatillo(Platillo platillo){
		cnx.ejecutarSentencia("DELETE FROM Platillo WHERE idPlatillo="+platillo.getIdPlatillo());
		actualizarListaDePlatillos();
	}
	/**
	*	Este metodo nos sirve para poder agregar un platillo.
	*	@param platillo Este es el platillo que en este caso se va a agregar.
	*/
	public void agregarPlatillo(Platillo platillo){
		cnx.ejecutarSentencia("INSERT INTO Platillo(nombre,precio) VALUES ('"+platillo.getNombre()+"',"+platillo.getPrecio()+")");
		actualizarListaDePlatillos();
	}
	/**
	*	Este metodo nos sirve para poder modificar un platillo.
	*	@param platillo Este es el platillo que en este caso se va a modificar.
	*/
	public void modificarPlatillo(Platillo platillo){
		cnx.ejecutarSentencia("UPDATE Platillo SET nombre='"+platillo.getNombre()+"', precio="+platillo.getPrecio()+" WHERE idPlatillo="+platillo.getIdPlatillo());
		actualizarListaDePlatillos();
	}
}

