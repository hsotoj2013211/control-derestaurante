/**
*	Esta clase es un beans de Usuario.
*	@author Hermes Sotoj
*/
package org.hermes.beans;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;

public class DetalleOrden{
	private IntegerProperty idPedido, idOrden;
	/**
	*	Este es el constructor de la clase se ejecuta cada vez que se instancia la clase.
	*/
	public DetalleOrden(){
		idPedido = new SimpleIntegerProperty();
		idPedido = new SimpleIntegerProperty();
		
	}
	/**
	*	Este es el constructor de la clase se ejecuta cada vez que se instancia la clase.
	*	@param idUsuario Esta variable se encarga de almacenar el id del Usuario
	*	@param idModulo Esta variable se encarga de almacenar el id del modulo al que pertenece este usuario
	*	@param nombre Esta variable se encarga de almacenar el nombre del usuario
	*	@param pass Esta variable se encarga de almacenar la contraseña del usuario
	*	@param edad Esta variable se encarga de almacenar la edad del usuario
	*	@param nick Esta variable se encarga de almacenar el nick del usuario
	*	@param nombreModulo Esta variable se encarga de almacenar el
	*/
	public DetalleOrden(int idPedido,int idOrden){
		this.idPedido = new SimpleIntegerProperty(idPedido);
		this.idOrden = new SimpleIntegerProperty(idOrden);
	}
	/**
	*	Este metodo nos devuelve el id de un usuario
	*	@return idUsuario Contiene el valor del id de un Usuario
	*/
	public int getIdPedido(){
		return idPedido.get();
	}
	/**
	*	Este metodo nos sirve para almacenar el valor del id del Usuario
	*	@param idUsuario Esta variable guarda el valor del id de un Usuario
	*/
	public void setIdPedido(int idPedido){
		this.idPedido.set(idPedido);
	}
	/**
	*	Este metodo nos devuelve el id de un usuario
	*	@return idUsuario Contiene el valor del id de un Usuario
	*/
	public IntegerProperty idPedidoProperty(){
		return idPedido;
	}

	public int getIdOrden(){
		return idOrden.get();
	}
	/**
	*	Este metodo nos sirve para almacenar el valor del id del Usuario
	*	@param idUsuario Esta variable guarda el valor del id de un Usuario
	*/
	public void setIdOrden(int idOrden){
		this.idOrden.set(idOrden);
	}
	/**
	*	Este metodo nos devuelve el id de un usuario
	*	@return idUsuario Contiene el valor del id de un Usuario
	*/
	public IntegerProperty idPlatilloProperty(){
		return idOrden;
	}
}
