/**
*
*/
package org.hermes.manejadores;

import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import org.hermes.db.Conexion;
import org.hermes.beans.DetallePlatillo;

public class ManejadorDetallePlatillo{
	private Conexion cnx;
	private ObservableList<DetallePlatillo> listaDeIngredienteExtra;
	/**
	*	Este es el constructor de la clase ManejadorIngrediente se ejecuta cada vez que se instancia la clase.
	*	@param cnx Esta variable es la que se encarga de recivir la conexion a la base de datos.
	*/
	public ManejadorDetallePlatillo(Conexion cnx){
		this.cnx=cnx;
		this.listaDeIngredienteExtra=FXCollections.observableArrayList();
	}
	/**
	*	Este metodo nos sirve para poder actualizar la lista cada vez que se hace un cambio en la base de datos.
	*/
	public void actualizarListaDeIngredienteExtra(){
		listaDeIngredienteExtra.clear();
		ResultSet resultado = cnx.ejecutarConsulta("SELECT idIngrediente, idPlatillo FROM DetallePlatillo");
		try{
			while(resultado.next()){
				DetallePlatillo orden = new DetallePlatillo(resultado.getInt("idPlatillo"),resultado.getInt("idIngrediente"));
				listaDeIngredienteExtra.add(orden);
			}
		}catch(SQLException sql){
			sql.printStackTrace();
		}
	}
	/**
	*	Este metodo nos sirve para poder actuarlizar la lista de ingredientes con los datos de la base de datos.
	*	@return listaDeDetalleOrden Devuelve la lista de ingredientes ya actualizadas.
	*/
	public ObservableList<DetallePlatillo> getListaIngredienteExtra(){
		actualizarListaDeIngredienteExtra();
		return listaDeIngredienteExtra;
	}
	/**
	*	Este metodo sirve para agregar un ingrediente.
	*	@param ingrediente es el ingrediente que en este caso se va a agregar
	*/
	public void agregarOrden(DetallePlatillo orden){
		cnx.ejecutarSentencia("INSERT INTO DetallePlatillo(idPlatillo,idIngrediente) VALUES ("+orden.getIdPlatillo()+" , "+orden.getIdIngrediente()+")");
		actualizarListaDeIngredienteExtra();
	}
}

