/**
*	Esta clase es un beans de Usuario.
*	@author Hermes Sotoj
*/
package org.hermes.beans;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;

public class Orden{
	private IntegerProperty idOrden, idEstado, gastoTotal;
	private StringProperty nombre;
	/**
	*	Este es el constructor de la clase se ejecuta cada vez que se instancia la clase.
	*/
	public Orden(){
		idOrden = new SimpleIntegerProperty();
		idEstado = new SimpleIntegerProperty();
		nombre = new SimpleStringProperty();
		gastoTotal = new SimpleIntegerProperty();
	}
	/**
	*	Este es el constructor de la clase se ejecuta cada vez que se instancia la clase.
	*	@param idUsuario Esta variable se encarga de almacenar el id del Usuario
	*	@param idModulo Esta variable se encarga de almacenar el id del modulo al que pertenece este usuario
	*	@param nombre Esta variable se encarga de almacenar el nombre del usuario
	*/
	public Orden(int idOrden, String nombre, int idEstado, int gastoTotal){
		this.idOrden = new SimpleIntegerProperty(idOrden);
		this.nombre = new SimpleStringProperty(nombre);
		this.idEstado = new SimpleIntegerProperty(idEstado);
		this.gastoTotal = new SimpleIntegerProperty(gastoTotal);
	}
	/**
	*	Este metodo nos devuelve el id de un usuario
	*	@return idUsuario Contiene el valor del id de un Usuario
	*/
	public int getIdOrden(){
		return idOrden.get();
	}
	/**
	*	Este metodo nos sirve para almacenar el valor del id del Usuario
	*	@param idUsuario Esta variable guarda el valor del id de un Usuario
	*/
	public void setIdOrden(int idOrden){
		this.idOrden.set(idOrden);
	}
	/**
	*	Este metodo nos devuelve el id de un usuario
	*	@return idUsuario Contiene el valor del id de un Usuario
	*/
	public IntegerProperty idOrdenProperty(){
		return idOrden;
	}

	public int getIdEstado(){
		return idEstado.get();
	}
	/**
	*	Este metodo nos sirve para almacenar el valor del id del Usuario
	*	@param idUsuario Esta variable guarda el valor del id de un Usuario
	*/
	public void setIdEstado(int idEstado){
		this.idEstado.set(idEstado);
	}
	/**
	*	Este metodo nos devuelve el id de un usuario
	*	@return idUsuario Contiene el valor del id de un Usuario
	*/
	public IntegerProperty idEstadoProperty(){
		return idEstado;
	}
	
	public String getNombre(){
		return nombre.get();
	}
	/**
	*	Este metodo es el que almacena el nombre del usuario
	*	@param nombre Es la variable que contiene el valor del nombre del usuario.
	*/
	public void setNombre(String nombre){
		this.nombre.set(nombre);
	}
	/**
	*	Este metodo es el que devuelve el nombre del Usuario
	*	@return nombre Esta variable es la que se devuelve la cual contiene el nombre del usuario
	*/
	public StringProperty nombreProperty(){
		return nombre;
	}
	public int getTotal(){
		return gastoTotal.get();
	}
	/**
	*	Este metodo nos sirve para almacenar el valor del id del Usuario
	*	@param idUsuario Esta variable guarda el valor del id de un Usuario
	*/
	public void setTotal(int gastoTotal){
		this.gastoTotal.set(gastoTotal);
	}
	/**
	*	Este metodo nos devuelve el id de un usuario
	*	@return idUsuario Contiene el valor del id de un Usuario
	*/
	public IntegerProperty TotalProperty(){
		return gastoTotal;
	}
}
