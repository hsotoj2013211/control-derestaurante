/**
*	Esta clase es un beans de Platillo.
*	@author Hermes Sotoj
*/
package org.hermes.beans;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;

public class Platillo{
	private IntegerProperty idPlatillo, costo;
	private StringProperty nombre;
	/**
	*	Este es el constructor de la clase Platillo se ejecuta cada vez que se instancia la clase
	*/
	public Platillo(){
		idPlatillo = new SimpleIntegerProperty();
		costo = new SimpleIntegerProperty();
		nombre = new SimpleStringProperty("");
	}
	/**
	*	Este es otro constructor solo que este si pide parametros.
	*	@param idPlatillo es el id del Platillo
	*	@param costo Es el precio del platilo
	*	@param nombre Es el nombre del platillo
	*/
	public Platillo(int idPlatillo, int costo, String nombre){
		this.idPlatillo = new SimpleIntegerProperty(idPlatillo);
		this.costo = new SimpleIntegerProperty(costo);
		this.nombre = new SimpleStringProperty(nombre);
	}
	/**
	*	Este metodo nos devuelve el id de Platillo.
	*	@return idPlatillo Es la variable del id de Platillo
	*/
	public int getIdPlatillo(){
		return idPlatillo.get();
	}
	/**
	*	Este metodo es el que almacena el id del Platillo.
	*	@param idPlatillo es la que contiene el id del platillo y la cual se va a almacenar.
	*/
	public void setIdPlatillo(int idPlatillo){
		this.idPlatillo.set(idPlatillo);
	}
	/**
	*	Este metodo nos devuelve el id de Platillo.
	*	@return idPlatillo Es la variable del id de Platillo
	*/
	public IntegerProperty idPlatilloProperty(){
		return idPlatillo;
	}
	/**
	*	Este metodo nos devuelve el precio de un platillo en una variable de tipo int.
	*	@return costo Es la variable de tipo int que contiene el precio del platillo.
	*/
	public int getPrecio(){
		return costo.get();
	}
	/**
	*	Este metodo es el que Almacena el precio del Platillo.
	*	@param costo Es la variable de tipo int la cual es almacenada.
	*/
	public void setPrecio(int costo){
		this.costo.set(costo);
	}
	/**
	*	Este metodo nos devuelve el precio de un platillo.
	*	@return costo Es la variable que contiene el precio del platillo.
	*/
	public IntegerProperty PrecioProperty(){
		return costo;
	}
	/**
	*	Este metodo nos duvuelve el nombre en una variable de tipo String.
	*	@return nombre Es la variable que es devuelta la cual contiene el nombre del platillo
	*/
	public String getNombre(){
		return nombre.get();
	}
	/**
	*	Este metodo almacena el nombre del platillo.
	*	@param nombre Es la variable que se va a guardar.
	*/
	public void setNombre(String nombre){
		this.nombre.set(nombre);
	}
	/**
	*	Este metodo nos devuelve el nombre del platillo.
	*	@return nombre Devuelve el nombre del Platillo
	*/
	public StringProperty nombreProperty(){
		return nombre;
	}
}
