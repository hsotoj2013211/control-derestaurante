/**
*
*/
package org.hermes.manejadores;

import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import org.hermes.db.Conexion;
import org.hermes.beans.DetalleOrden;

public class ManejadorDetalleOrden{
	private Conexion cnx;
	private ObservableList<DetalleOrden> listaDeDetalleOrden;
	/**
	*	Este es el constructor de la clase ManejadorIngrediente se ejecuta cada vez que se instancia la clase.
	*	@param cnx Esta variable es la que se encarga de recivir la conexion a la base de datos.
	*/
	public ManejadorDetalleOrden(Conexion cnx){
		this.cnx=cnx;
		this.listaDeDetalleOrden=FXCollections.observableArrayList();
	}
	/**
	*	Este metodo nos sirve para poder actualizar la lista cada vez que se hace un cambio en la base de datos.
	*/
	public void actualizarListaDeOrdenesPedidos(){
		listaDeDetalleOrden.clear();
		ResultSet resultado = cnx.ejecutarConsulta("SELECT idOrden, idPedido FROM DetalleOrden");
		try{
			while(resultado.next()){
				DetalleOrden orden = new DetalleOrden(resultado.getInt("idOrden"),resultado.getInt("idPedido"));
				listaDeDetalleOrden.add(orden);
			}
		}catch(SQLException sql){
			sql.printStackTrace();
		}
	}
	/**
	*	Este metodo nos sirve para poder actuarlizar la lista de ingredientes con los datos de la base de datos.
	*	@return listaDeDetalleOrden Devuelve la lista de ingredientes ya actualizadas.
	*/
	public ObservableList<DetalleOrden> getListaOrdenes(){
		actualizarListaDeOrdenesPedidos();
		return listaDeDetalleOrden;
	}
	/**
	*	Este metodo sirve para agregar un ingrediente.
	*	@param ingrediente es el ingrediente que en este caso se va a agregar
	*/
	public void agregarOrden(DetalleOrden orden){
		cnx.ejecutarSentencia("INSERT INTO DetalleOrden(idOrden,idPedido) VALUES ("+orden.getIdOrden()+" , "+orden.getIdPedido()+")");
		actualizarListaDeOrdenesPedidos();
	}
}

