CREATE DATABASE dbRestaurante2013211
GO
USE dbRestaurante2013211
GO
CREATE TABLE Platillo(
	idPlatillo INT IDENTITY (1,1),
	nombre VARCHAR (255) NOT NULL,
	precio INT,
	PRIMARY KEY (idPlatillo) 
)
CREATE TABLE Factura(
	idFactura INT IDENTITY (1,1),
	total INT NOT NULL,
	PRIMARY KEY (idFactura) 
)
CREATE TABLE Ingrediente(
	idIngrediente INT IDENTITY (1,1),
	nombre VARCHAR (255) NOT NULl,
	precio INT,
	PRIMARY KEY (idIngrediente) 
)
CREATE TABLE Modulo(
	idModulo INT IDENTITY (1,1),
	nombre VARCHAR (255) NOT NULL,
	PRIMARY KEY (idModulo)
)
CREATE TABLE Usuario(
	idUsuario INT IDENTITY (1,1),
	idModulo INT,
	nombre VARCHAR (255) NOT  NULL,
	nick VARCHAR(233)NOT NULL,
	edad NUMERIC (3) NOT NULL,
	pass VARCHAR (255) NOT NULL,
	PRIMARY KEY (idUsuario),
	FOREIGN KEY(idModulo) REFERENCES Modulo(idModulo)
)
CREATE TABLE Estado(
	idEstado INT IDENTITY (1,1),
	nombre VARCHAR (255) NOT NULL,
	PRIMARY KEY (idEstado)
)
CREATE TABLE Orden(
	idOrden INT IDENTITY (1,1),
	nombre VARCHAR (255) NOT NULL,
	idEstado INT,
	gastoTotal INT,
	FOREIGN KEY(idEstado) REFERENCES Estado(idEstado),
	PRIMARY KEY (idOrden)
)
CREATE TABLE Pedido(
	idPedido INT IDENTITY (1,1),
	nombre	VARCHAR (255) NOT NULL,
	idOrden INT NOT NULL,
	totalPedido INT,
	FOREIGN KEY(idOrden) REFERENCES Orden(idOrden),
	PRIMARY KEY (idPedido)
)
CREATE TABLE Mesa(
	idMesa INT IDENTITY (1,1),
	categoria VARCHAR (255) NOT NULL,
	PRIMARY KEY (idMesa)
)
CREATE TABLE Cliente(
	idCliente INT IDENTITY (1,1),
	nombre VARCHAR (255) NOT NULL,
	PRIMARY KEY (idCliente)
)
CREATE TABLE DetallePlatillo(
	idPlatillo INT NOT NULL,
	idIngrediente INT NOT NULL,
	cantidad INT NOT NULL,
	PRIMARY KEY (idPlatillo, idIngrediente),
	FOREIGN KEY (idPlatillo) REFERENCES Platillo (idPlatillo),
	FOREIGN KEY (idIngrediente) REFERENCES Ingrediente (idIngrediente)
)
CREATE TABLE DetallePedido(
	idPedido INT NOT NULL,
	idPlatillo INT NOT NULL,
	cantidad INT NOT NULL,
	PRIMARY KEY(idPedido,idPlatillo),
	FOREIGN KEY(idPedido) REFERENCES Pedido(idPedido),
	FOREIGN key(idPlatillo) REFERENCES Platillo(idPlatillo)
)
CREATE TABLE DetalleFactura(
	idFactura INT NOT NULL,
	idOrden INT NOT NULL,
	PRIMARY KEY (idFactura, idOrden),
	FOREIGN KEY (idFactura) REFERENCES Factura (idFactura),
	FOREIGN KEY (idOrden) REFERENCES Orden (idOrden)
)

INSERT INTO Platillo(nombre, precio) VALUES('Pizza', 200)
INSERT INTO Platillo(nombre, precio) VALUES('lasagna', 159)
INSERT INTO Platillo(nombre, precio) VALUES('Pasta', 140)

INSERT INTO Ingrediente(nombre, precio) VALUES('Queso', 15)
INSERT INTO Ingrediente(nombre, precio) VALUES('Fideos', 14)
INSERT INTO Ingrediente(nombre, precio) VALUES('Carne', 20)
INSERT INTO Ingrediente(nombre, precio) VALUES('Harina', 13)

INSERT INTO Modulo(nombre) VALUES ('Chef')
INSERT INTO Modulo(nombre) VALUES ('Mesero')
INSERT INTO Modulo(nombre) VALUES ('Administrador')

INSERT INTO Usuario(idModulo, nombre, edad, pass, nick) VALUES (1, 'Hermes', 15, '123', 'Miguel')
INSERT INTO Usuario(idModulo, nombre, edad, pass, nick) VALUES (2, 'Paniagua', 18, '1234', 'Mesero')
INSERT INTO Usuario(idModulo, nombre, edad, pass, nick) VALUES (3, 'Ricardo', 15, '12345','Admin')

INSERT INTO Estado(nombre) VALUES ('Espera')
INSERT INTO Estado(nombre) VALUES ('Cancelada')
INSERT INTO Estado(nombre) VALUES ('Pagada')
INSERT INTO Estado(nombre) VALUES ('Entregada')

INSERT INTO Orden(nombre,idEstado) VALUES('Primera Orden',1)
INSERT INTO Orden(nombre,idEstado) VALUES('Segunda Orden',1)
INSERT INTO Orden(nombre,idEstado) VALUES('Tercera Orden',1)

SELECT * FROM Usuario
SELECT * FROM Platillo
SELECT * FROM Ingrediente
SELECT * FROM Modulo
SELECT * FROM Orden
SELECT * FROM Pedido

INSERT INTO Pedido(nombre,idOrden) VALUES ('asd',1)

GO
CREATE TRIGGER ActualizarTotalPedido
ON DetallePedido
AFTER INSERT
AS
BEGIN
	DECLARE	@cantidadPlatillo INT, @idPedido INT, @idPlatillo INT, @precio INT
	SET @cantidadPlatillo=(SELECT TOP 1 cantidad FROM inserted)
	SET @idPedido=(SELECT TOP 1 idPedido FROM inserted)
	SET @idPlatillo=(SELECT TOP 1 idPlatillo FROM inserted)
	SET @precio=(SELECT TOP 1 precio FROM Platillo INNER JOIN inserted ON Platillo.idPlatillo=inserted.idPlatillo WHERE Platillo.idPlatillo=@idPlatillo)
	UPDATE Pedido SET totalPedido=totalPedido+(@cantidadPlatillo*@precio) WHERE idPedido=@idPedido
END

GO 
CREATE TRIGGER ActualizarTotalOrden
ON Pedido	
AFTER UPDATE
AS
BEGIN 
	DECLARE @total INT, @idOrden INT
	SET @total=(SELECT TOP 1 totalPedido FROM inserted)
	SET @idOrden=(SELECT TOP 1 idOrden FROM inserted WHERE idOrden=@idOrden)
	UPDATE Orden SET gastoTotal=gastoTotal+(@total) WHERE idOrden=@idOrden
END
