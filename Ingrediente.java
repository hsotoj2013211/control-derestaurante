/**
*	Esta clase es el beans de Ingrediente aqui se guarda todo con respecto a un ingrediente.
*	@return Hermes Sotoj
*/
package org.hermes.beans;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;

public class Ingrediente{
	private IntegerProperty idIngrediente, costo;
	private StringProperty nombre;
	/**
	*	Este es el constructor se ejecuta cada ves que se instancia esta clase.
	*/
	public Ingrediente(){
		idIngrediente = new SimpleIntegerProperty();
		costo = new SimpleIntegerProperty();
		nombre = new SimpleStringProperty("");
	}
	/**
	*	Este es un constructor el cual pide parametros.
	*	@param idIngrediente Esta variable es la que almacena el id del Ingrediente.
	*   @param costo Esta variable es la que almacena el costo del ingrediente.
	*	@param nombre Esta variable es la que almacena el nombre del ingrediente.
	*/
	public Ingrediente(int idIngrediente, int costo, String nombre){
		this.idIngrediente = new SimpleIntegerProperty(idIngrediente);
		this.costo = new SimpleIntegerProperty(costo);
		this.nombre = new SimpleStringProperty(nombre);
	}
	/**
	*	Este metodo es el que devuelve el id de un Ingrediente.
	*	@return idIngrediente Devuelve el valor del id del ingrediente.
	*/
	public int getIdIngrediente(){
		return idIngrediente.get();
	}
	/**
	*	Este metodo es el que almacena el valor del id del Ingrediente.
	*	@param idIngrediente En esta variable se almacena el valor del id.
	*/
	public void setIdIngrediente(int idIngrediente){
		this.idIngrediente.set(idIngrediente);
	}
	/**
	*	Este metodo devuelve el id del ingrediente.
	*	@return idIngrediente Devuelve el id del ingrediente.
	*/
	public IntegerProperty idIngredienteProperty(){
		return idIngrediente;
	}
	/**
	*	Este metodo devuelve el precio del ingrediente como tipo entero.
	*	@return costo Devuelve el precio del ingrediente como tipo entero.
	*/
	public int getPrecio(){
		return costo.get();
	}
	/**
	*	Este metodo almacena el precio del ingrediente.
	*	@param costo Almacena el precio del ingrediente.
	*/
	public void setPrecio(int costo){
		this.costo.set(costo);
	}
	/**
	*	Este metodo devuelve el costo del ingrediente.
	*	@return costo Devuelve el costo del ingrediente.
	*/
	public IntegerProperty PrecioProperty(){
		return costo;
	}
	/**
	*	Este metodo devuelve el nombre del ingrediente como String.
	*	@return nombre Devuelve el nombre del ingrediente.
	*/
	public String getNombre(){
		return nombre.get();
	}
	/**
	*	Este metodo almacena el nombre del ingrediente.
	*	@param nombre Almacena el nombre del ingrediente.
	*/
	public void setNombre(String nombre){
		this.nombre.set(nombre);
	}
	/**
	*	Este metodo devuelve el valor del nombre de un ingrediente.
	* 	@return nombre Es el nombre del ingrediente.
	*/
	public StringProperty nombreProperty(){
		return nombre;
	}
}
