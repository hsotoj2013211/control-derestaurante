/**
*
*/
package org.hermes.manejadores;

import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import org.hermes.db.Conexion;
import org.hermes.beans.DetallePedido;

public class ManejadorDetallePedido{
	private Conexion cnx;
	private ObservableList<DetallePedido> listaDetallePedido;
	/**
	*	Este es el constructor de la clase ManejadorIngrediente se ejecuta cada vez que se instancia la clase.
	*	@param cnx Esta variable es la que se encarga de recivir la conexion a la base de datos.
	*/
	public ManejadorDetallePedido(Conexion cnx){
		this.cnx=cnx;
		this.listaDetallePedido=FXCollections.observableArrayList();
	}
	/**
	*	Este metodo nos sirve para poder actualizar la lista cada vez que se hace un cambio en la base de datos.
	*/
	public void actualizarListaDePedidoPlatillo(){
		listaDetallePedido.clear();
		ResultSet resultado = cnx.ejecutarConsulta("SELECT idPlatillo, cantidad, idPedido FROM DetallePedido");
		try{
			while(resultado.next()){
				DetallePedido orden = new DetallePedido(resultado.getInt("idPlatillo"),resultado.getInt("idPedido"), resultado.getInt("cantidad"));
				listaDetallePedido.add(orden);
			}
		}catch(SQLException sql){
			sql.printStackTrace();
		}
	}
	/**
	*	Este metodo nos sirve para poder actuarlizar la lista de ingredientes con los datos de la base de datos.
	*	@return listaDetallePedido Devuelve la lista de ingredientes ya actualizadas.
	*/
	public ObservableList<DetallePedido> getListaOrdenes(){
		actualizarListaDePedidoPlatillo();
		return listaDetallePedido;
	}
	/**
	*	Este metodo sirve para agregar un ingrediente.
	*	@param ingrediente es el ingrediente que en este caso se va a agregar
	*/
	public void agregarOrden(DetallePedido orden){
		cnx.ejecutarSentencia("INSERT INTO DetallePedido(idPedido,idPlatillo, cantidad) VALUES ("+orden.getIdPedido()+" , "+orden.getIdPlatillo()+" , "+orden.getCantidad()+")");
		actualizarListaDePedidoPlatillo();
	}
}

