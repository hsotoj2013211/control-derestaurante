/**
*	Esta clase es la Principal la que contiene el metodo main (Aqui empieza el programa)
*	@author Hermes Sotoj
*/
package org.hermes.sistema;

import org.hermes.ui.App;

import javafx.application.Application;

public class Principal{
	/**
	*	Este metodo es el main con el que empieza el programa.
	*/
	public static void main(String args[]){
		Application.launch(App.class, args);
	}
}
