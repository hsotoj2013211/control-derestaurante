/**
*	Esta clase es un beans de Usuario.
*	@author Hermes Sotoj
*/
package org.hermes.beans;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;

public class DetallePlatillo{
	private IntegerProperty idIngrediente, idPlatillo;
	/**
	*	Este es el constructor de la clase se ejecuta cada vez que se instancia la clase.
	*/
	public DetallePlatillo(){
		idIngrediente = new SimpleIntegerProperty();
		idPlatillo = new SimpleIntegerProperty();
		
	}
	/**
	*	Este es el constructor de la clase se ejecuta cada vez que se instancia la clase.
	*	@param idUsuario Esta variable se encarga de almacenar el id del Usuario
	*	@param idModulo Esta variable se encarga de almacenar el id del modulo al que pertenece este usuario
	*	@param nombre Esta variable se encarga de almacenar el nombre del usuario
	*	@param pass Esta variable se encarga de almacenar la contraseña del usuario
	*	@param edad Esta variable se encarga de almacenar la edad del usuario
	*	@param nick Esta variable se encarga de almacenar el nick del usuario
	*	@param nombreModulo Esta variable se encarga de almacenar el
	*/
	public DetallePlatillo(int idIngrediente,int idPlatillo){
		this.idIngrediente = new SimpleIntegerProperty(idIngrediente);
		this.idPlatillo = new SimpleIntegerProperty(idPlatillo);

	}
	/**
	*	Este metodo nos devuelve el id de un usuario
	*	@return idUsuario Contiene el valor del id de un Usuario
	*/
	public int getIdIngrediente(){
		return idIngrediente.get();
	}
	/**
	*	Este metodo nos sirve para almacenar el valor del id del Usuario
	*	@param idUsuario Esta variable guarda el valor del id de un Usuario
	*/
	public void setIdIngrediente(int idIngrediente){
		this.idIngrediente.set(idIngrediente);
	}
	/**
	*	Este metodo nos devuelve el id de un usuario
	*	@return idUsuario Contiene el valor del id de un Usuario
	*/
	public IntegerProperty idPedidoProperty(){
		return idIngrediente;
	}

	public int getIdPlatillo(){
		return idPlatillo.get();
	}
	/**
	*	Este metodo nos sirve para almacenar el valor del id del Usuario
	*	@param idUsuario Esta variable guarda el valor del id de un Usuario
	*/
	public void setIdPlatillo(int idPlatillo){
		this.idPlatillo.set(idPlatillo);
	}
	/**
	*	Este metodo nos devuelve el id de un usuario
	*	@return idUsuario Contiene el valor del id de un Usuario
	*/
	public IntegerProperty idPlatilloProperty(){
		return idPlatillo;
	}
	
}
