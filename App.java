/**
* Esta clase es la que contiene la interfaz grafica de todo el programa.
* @author Hermes Sotoj
*/
package org.hermes.ui;

import javafx.application.Application;

import javafx.stage.Stage;
import javafx.stage.Modality;

import javafx.scene.control.TabPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.control.TableView;
import javafx.scene.control.Button;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.effect.ImageInput;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.ToolBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Button;

import javafx.collections.ObservableList;

import javafx.event.EventHandler;
import javafx.event.Event;
import javafx.event.ActionEvent;

import javafx.beans.property.SimpleListProperty;

import java.util.ArrayList;

import org.hermes.beans.Usuario;
import org.hermes.beans.Platillo;
import org.hermes.beans.Pedido;
import org.hermes.beans.Orden;
import org.hermes.beans.DetallePlatillo;
import org.hermes.beans.DetalleOrden;
import org.hermes.beans.DetallePedido;
import org.hermes.beans.Ingrediente;
import org.hermes.manejadores.ManejadorUsuario;
import org.hermes.manejadores.ManejadorPlatillo;
import org.hermes.manejadores.ManejadorIngrediente;
import org.hermes.manejadores.ManejadorOrden;
import org.hermes.manejadores.ManejadorPedido;
import org.hermes.manejadores.ManejadorDetallePedido;
import org.hermes.manejadores.ManejadorDetalleOrden;
import org.hermes.manejadores.ManejadorDetallePlatillo;
import org.hermes.db.Conexion;

import javafx.geometry.Insets;

public class App extends Application implements EventHandler<Event>{
	private TextField tfNombre,tfNombreA,tfPrecioA,tfEdad, tfNick, tfModulo, tfNombreU, tfNombreModulo,tfNombreIngre,tfPrecioIngre;
	private TextField tfNombreOrden,tfEstadoOrden, tfIdPedido, tfCantidad, tfPlatillo;
	private TextField tfNombrePedido, tfCantidadPlatillo,tfIdOrden;
	private PasswordField pfPass, pfPassword;
	private Label lbNombre, lbPass, lbNombreA, lbPrecioA, lbIncorrecto, lbTitulo, lbNick, lbEdad, lbModulo, lbNombreModulo;
	private Label lbNombreOrden;
	private TabPane tpPrincipal;
	private Tab tbDeLogin, tbDeUsuario, tbPlatillo,tbUsuarios,tbIngrediente, tbCRUD,tbCRUDIngrediente ,tbMenu, tbMenuChef, tbCRUDUsuario,tbMenuMesero;
	private Tab tbOrden, tbPedido, tbDetallePedido, tbDetalleOrden;
	private Tab tbGenerarOrden, tbGenerarPedido, tbIngredienteExtra;
	private Tab tbCrearPedido, tbCrearPedidoPlatillo;
	private GridPane gpDeLogin, gpContentCRUD,gpContentCRUDIngrediente, gpMenu, gpMenuChef, gpContentCRUDUsuario, gpMenuMesero, gpContenedorUsuario,gpContenedorPlatillo, gpContenedorIngrediente;
	private GridPane gpOrden, gpPedido, gpDetallePedido, gpDetalleOrden;
	private GridPane gpGenerarOrden, gpGenerarPedido, gpCrearPedido,gpCrearPlatilloPedido;
	private BorderPane bpBotonesPrincipales;
	private Stage primaryStage;
	private TableView<Usuario> tvUsuarios;
	private TableView<Platillo> tvPlatillos;
	private TableView<Ingrediente> tvIngrediente;
	private TableView<Orden> tvOrden;
	private TableView<Pedido> tvPedido;
	private TableView<DetallePedido> tvDetallePedido;
	private ManejadorUsuario mUsuario;
	private ManejadorPlatillo mPlatillo;
	private ManejadorIngrediente mIngrediente;
	private ManejadorOrden mOrden;
	private ManejadorPedido mPedido;
	private ManejadorDetallePedido mDetallePedido;
	private ManejadorDetalleOrden mDetalleOrden;
	private ManejadorDetallePlatillo mDetallePlatillo;
	private Scene primaryScene;
	private Image imagen;
	private ImageView imagenTitulo;
	private Button btLogin, btnA, btnD, btnU, btnL,btnL1,btnL2, btnPlatillo,btnPlatillo1, btnUsuarios,btnOrden, btnAgregar, btnModificar, btnEliminar;
	private Button btnMenu,btnMenuUsu, btnIngrediente, btnIngrediente1, btnModificarUsuario;
	private Button btnAgregaIngrediente, btnModificarIngrediente, btnEliminarIngrediente, btnMenuIngrediente;
	private Button btnGenerarPedido, btnGenerarOrden,btnCambiarEstado, btnIngredienteExtra;
	private Button btnDesconectarOrden, btnMenuOrden, btnOrdenLista,btnPedidos,btnFacturarOrdenes;
	private Button btnAgregarIngredientePedido, btnAgregarIngredientePlatillo,btnAgregarDetalleIngredientePlatillo, btnListoPedido,btnCrearPedido,btnVerPedidos;
	private Button btnRegresarMenu, btnDeslogearse, btnDesconectarPedido, btnMenuPedido;
	private Conexion conexion;
	private ToolBar tbPrincipal, toolbUsuarios;
	private HBox hbPlatillo, hbUsuario, hbBebida, hbIngrediente;
	private boolean estadoMantenimiento, estadoMantenimientoIngre,  estadoMantenimientoUsurio,estadoMantenimientoOrden;
	private BorderPane bpPrincipal;
	private Platillo platilloModificar;
	private Usuario usuarioModificar;
	private Ingrediente ingredienteModificar;
	private Orden ordenModificar;
	private Pedido pedidoMdificar;
	private DetallePedido detallePedido;
	/**
	*	Este es el metodo start con este metodo se empieza la interfaz grafica.
	*  	@param primaryStage es una variable de tipo Stage y es la escena donde se guarda todo.
	*/
	public void start(Stage primaryStage){
		this.primaryStage=primaryStage;
		conexion = new Conexion();
		
		this.setMIngrediente(new ManejadorIngrediente(conexion));
		this.setMUsuario(new ManejadorUsuario(conexion));
		this.setMPlatillo(new ManejadorPlatillo(conexion));
		this.setMOrden(new ManejadorOrden(conexion));
		this.setMPedido(new ManejadorPedido(conexion));
		this.setMDetallePedido(new ManejadorDetallePedido(conexion));
		Image imagen=new Image("images.jpg");
		primaryScene = new Scene(this.getRoot());
		primaryStage.setScene(primaryScene);
		primaryStage.getIcons().add(imagen);
		primaryStage.setTitle("Control de Restaurante");
		primaryStage.setMinHeight(460);
		primaryStage.setMinWidth(630);
		primaryStage.setResizable(false);
		primaryStage.show();
	}
	/**
	*	Este metodo es el que guarda el BorderPane en la Tab Principal.
	*	@return bpPrincipal Devuelve el BorderPane principal	
	*/
	public BorderPane getRoot(){
		if(bpPrincipal==null){
			bpPrincipal = new BorderPane();
			bpPrincipal.setCenter(this.getTabPane());
		}
		return bpPrincipal;
	}
	/**
	*	Este metodo sirve para almacenar todas las tab en un tab Principal.
	*	@return tbPrincipal Devuelve la tab Principal.
	*/
	public TabPane getTabPane(){
		if(tpPrincipal == null){
			tpPrincipal = new TabPane();
			tpPrincipal.setStyle("-fx-background-image: url(comida.jpg);-fx-background-repeat: no-repeat");
			tpPrincipal.getTabs().add(this.getTabContent());
		}
		return tpPrincipal;
	}
	public Tab getTabPedidoPlatillo(){
		if(tbCrearPedidoPlatillo==null){
			tbCrearPedidoPlatillo= new Tab("Platillo Pedido");
			tbCrearPedidoPlatillo.setContent(this.getGridPanePedidoPlatillo());
		}
		return tbCrearPedidoPlatillo;
	}
	public GridPane getGridPanePedidoPlatillo(){
		if(gpCrearPlatilloPedido==null){
			gpCrearPlatilloPedido= new GridPane();
			tfIdPedido = new TextField();
			tfPlatillo = new TextField();
			tfCantidad = new TextField();
			lbNombre = new Label ("Escriba el id del Platillo: ");
			lbNombre.setStyle("-fx-background-color: #F5F5F5;");
			lbNombre.setPrefWidth(250);
			lbNombreA = new Label("Escriba la cantidad de Platillos:");
			lbNombreA.setPrefWidth(290);
			lbPrecioA = new Label("Escriba el id del Pedido: ");
			lbPrecioA.setPrefWidth(240);
			btnListoPedido = new Button("Listo!!");
			btnListoPedido.setPrefWidth(200);
			btnListoPedido.addEventHandler(ActionEvent.ACTION, this);
			gpCrearPlatilloPedido.add(lbNombre,0,0);
			gpCrearPlatilloPedido.add(tfPlatillo,1,0);
			gpCrearPlatilloPedido.add(lbNombreA,2,0);
			gpCrearPlatilloPedido.add(tfCantidad,3,0);
			gpCrearPlatilloPedido.add(lbPrecioA,4,0);
			gpCrearPlatilloPedido.add(tfIdPedido,5,0);
			gpCrearPlatilloPedido.add(btnListoPedido, 5,8);
			gpCrearPlatilloPedido.add(getContentPlatillos(),0,1,4,4);
		}
		return gpCrearPlatilloPedido;
	}
	/**
	*	Este metodo sirve para que los datos del GridPane se pasen a una Tab.
	*	@return tbDeLogin Devuelve la Tab ya con el GridPane del Login. 
	*/
	public Tab getTabContent(){
		if(tbDeLogin==null){
			tbDeLogin = new Tab("Login");
			tbDeLogin.setStyle("fx-background-color: #0000FF");
			tbDeLogin.setContent(this.getContentLogin());
		}
		
		return tbDeLogin;
	}
	public Tab getTabCrearPedido(){
		if(tbCrearPedido==null){
			tbCrearPedido=new Tab("Agregar Pedido");
			tbCrearPedido.setStyle("fx-background-color: #0000FF");
			tbCrearPedido.setContent(this.getContentCrearPedido());
		}
		return tbCrearPedido;
	}
	public GridPane getContentCrearPedido(){
		if(gpCrearPedido==null){
			gpCrearPedido=new GridPane();
			tfNombrePedido=new TextField();
			tfNombrePedido.addEventHandler(KeyEvent.KEY_RELEASED, this);
			lbNombre=new Label("Nombre del Pedido: ");
			tfIdOrden = new TextField();
			tfIdOrden.addEventHandler(KeyEvent.KEY_RELEASED, this);
			lbNombreA=new Label("Id de la orden:");
			gpCrearPedido.add(lbNombre,0,0);
			gpCrearPedido.add(tfNombrePedido,1,0);
			gpCrearPedido.add(lbNombreA,0,1);
			gpCrearPedido.add(tfIdOrden,1,1);
		}
		return gpCrearPedido;
	}	
	public Tab getTabOrden(){
		if(tbOrden==null){
			tbOrden = new Tab("Orden");
			tbOrden.setStyle("fx-background-color: #0000FF");
			tbOrden.setContent(this.getContentOrden());
		}
		return tbOrden;
	}
	public Tab getTabPedido(){
		if(tbPedido==null){
			tbPedido = new Tab("Pedido");
			tbPedido.setStyle("fx-background-color: #0000FF");
			tbPedido.setContent(getGridPanePedido());
		}
		return tbPedido;
	}
	public GridPane getGridPanePedido(){
		if(gpPedido==null){
			gpPedido = new GridPane();
			btnCrearPedido= new Button("Crear Pedido");
			btnCrearPedido.setPrefWidth(90);
			btnCrearPedido.addEventHandler(ActionEvent.ACTION, this);
			btnAgregarIngredientePlatillo= new Button("Agregar Platillos");
			btnAgregarIngredientePlatillo.setPrefWidth(120);
			btnAgregarIngredientePlatillo.addEventHandler(ActionEvent.ACTION, this);
			btnVerPedidos= new Button("Ver Pedidos");
			btnVerPedidos.setPrefWidth(90);
			btnVerPedidos.addEventHandler(ActionEvent.ACTION, this);
			btnRegresarMenu = new Button("Menu");
			btnRegresarMenu.setPrefWidth(90);
			btnRegresarMenu.addEventHandler(ActionEvent.ACTION, this);
			btnDeslogearse = new Button("Desconectar");
			btnDeslogearse.setPrefWidth(90);
			btnDeslogearse.addEventHandler(ActionEvent.ACTION, this);
			gpPedido.add(btnCrearPedido,0,0);
			gpPedido.add(btnAgregarIngredientePlatillo,1,0);
			gpPedido.add(btnVerPedidos,2,0);
			gpPedido.add(btnRegresarMenu, 3,0);
			gpPedido.add(btnDeslogearse, 4,0);
			gpPedido.add(getTableViewPedido(),0,1,10,10);
		}
		return gpPedido;
	}
	public Tab getTabDetallePedido(){
		if(tbDetallePedido==null){
			tbDetallePedido=new Tab("Pedido Platillo");
			tbDetallePedido.setContent(getGridPaneDetallePedido());
		}
		return tbDetallePedido;
	}	
	public GridPane getGridPaneDetallePedido(){
		if(gpDetallePedido==null){
			gpDetallePedido=new GridPane();
			lbNombre = new Label("ESTES SON LOS PLATILLOS Y LAS CANTIDADES DE LOS PEDIDOS: ");
			lbNombre.setStyle("-fx-background-color: #FFFAF0;");
			gpDetallePedido.add(lbNombre, 0,0, 10,1);
			gpDetallePedido.add(getTableViewDetallePedido(), 0,1,10,10);
		}
		return gpDetallePedido;
	}
	public TableView<DetallePedido> getTableViewDetallePedido(){
		if(tvDetallePedido==null){
			tvDetallePedido = new TableView<DetallePedido>();
			tvDetallePedido.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
			TableColumn<DetallePedido, Integer> columnaPedido = new TableColumn<DetallePedido, Integer>("ID PEDIDO");
			columnaPedido.setCellValueFactory(new PropertyValueFactory<DetallePedido, Integer>("idPedido"));
			
			TableColumn<DetallePedido, Integer> columnaPlatillo = new TableColumn<DetallePedido, Integer>("ID PEDIDO");
			columnaPlatillo.setCellValueFactory(new PropertyValueFactory<DetallePedido, Integer>("idPlatillo"));
			
			TableColumn<DetallePedido, Integer> columnaCantidad = new TableColumn<DetallePedido, Integer>("ID PEDIDO");
			columnaCantidad.setCellValueFactory(new PropertyValueFactory<DetallePedido, Integer>("cantidad"));
			tvDetallePedido.getColumns().setAll(columnaPedido,columnaPlatillo,columnaCantidad);
			tvDetallePedido.setItems(mDetallePedido.getListaOrdenes());
		}
		return tvDetallePedido;
	}
	public TableView<Pedido> getTableViewPedido(){
		if(tvPedido==null){
			tvPedido = new TableView<Pedido>();
			
			tvPedido.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
			
			TableColumn<Pedido, Integer> columnaIdentificador = new TableColumn<Pedido, Integer>("IDENTIFICADOR DEL PEDIDO");
			columnaIdentificador.setCellValueFactory(new PropertyValueFactory<Pedido, Integer>("idPedido"));
			
			TableColumn<Pedido, Integer> columnaIdOrden = new TableColumn<Pedido, Integer>("ID ORDEN");
			columnaIdOrden.setCellValueFactory(new PropertyValueFactory<Pedido, Integer>("idOrden"));

			TableColumn<Pedido, String> columnaNombre = new TableColumn<Pedido, String>("NOMBRE DEL PEDIDO");
			columnaNombre.setCellValueFactory(new PropertyValueFactory<Pedido, String>("nombre"));
			
			tvPedido.getColumns().setAll(columnaIdentificador,columnaNombre,columnaIdOrden );
			
			tvPedido.setItems(mPedido.getListaPedidos());
		}
		return tvPedido;
	}
	public GridPane getContentOrden(){
		if(gpOrden==null){
			gpOrden=new GridPane();
			imagenTitulo=new ImageView("estados.png");
			btnGenerarOrden=new Button("Generar Orden");
			btnGenerarOrden.addEventHandler(ActionEvent.ACTION,this);
			btnCambiarEstado=new Button("Cambiar Estado");
			btnCambiarEstado.addEventHandler(ActionEvent.ACTION,this);
			btnDesconectarOrden = new Button("Desconectar");
			btnDesconectarOrden.addEventHandler(ActionEvent.ACTION,this);
			btnMenuOrden = new Button("Menu");
			btnMenuOrden.addEventHandler(ActionEvent.ACTION,this);
			
			btnFacturarOrdenes=new Button("Facturar");
			btnFacturarOrdenes.addEventHandler(ActionEvent.ACTION,this);
			
			gpOrden.add(btnGenerarOrden,0,0);
			gpOrden.add(btnDesconectarOrden,1,0);
			gpOrden.add(btnMenuOrden,2,0);
			gpOrden.add(btnCambiarEstado,3,0);
			gpOrden.add(btnFacturarOrdenes,4,0);
			gpOrden.add(getTableViewOrden(),0,1,5,5);
			gpOrden.add(imagenTitulo, 4,1,4,5);
		}
		return gpOrden;
	}
	public TableView<Orden> getTableViewOrden(){
		if(tvOrden==null){
			tvOrden = new TableView<Orden>();
			
			tvOrden.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
			
			TableColumn<Orden, Integer> columnaIdentificador = new TableColumn<Orden, Integer>("IDENTIFICADOR DE LA ORDEN");
			columnaIdentificador.setCellValueFactory(new PropertyValueFactory<Orden, Integer>("idOrden"));

			TableColumn<Orden, String> columnaNombre = new TableColumn<Orden, String>("NOMBRE DE LA ORDEN");
			columnaNombre.setCellValueFactory(new PropertyValueFactory<Orden, String>("nombre"));

			TableColumn<Orden, Integer> columnaPrecio = new TableColumn<Orden, Integer>("ESTADO DE LA ORDEN");
			columnaPrecio.setCellValueFactory(new PropertyValueFactory<Orden, Integer>("idEstado"));
			
			TableColumn<Orden, Integer> columnaTotal = new TableColumn<Orden, Integer>("GASTADO");
			columnaTotal.setCellValueFactory(new PropertyValueFactory<Orden, Integer>("gastoTotal"));
			
			tvOrden.getColumns().setAll(columnaIdentificador,columnaNombre,columnaPrecio, columnaTotal);
			
			tvOrden.setItems(mOrden.getListaOrdenes());
		}
		return tvOrden;
	}
	public Tab getTabGenerarOrden(){
		if(tbGenerarOrden==null){
			tbGenerarOrden = new Tab("Generar Orden");
			tbGenerarOrden.setStyle("fx-background-color: #0000FF");
			tbGenerarOrden.setContent(getContenidoGenerarOrden());
		}
		return tbGenerarOrden;
	}
	public GridPane getContenidoGenerarOrden(){
		if(gpGenerarOrden==null){
			gpGenerarOrden= new GridPane();
			tfNombreOrden= new TextField();
			tfNombreOrden.addEventHandler(KeyEvent.KEY_RELEASED, this);
			tfEstadoOrden = new TextField();
			tfEstadoOrden.addEventHandler(KeyEvent.KEY_RELEASED, this);
			lbNombreOrden= new Label("Escriba el nombre de la orden: ");
			lbNombre= new Label("Escriba el estado de la orden: ");
			gpGenerarOrden.add(lbNombreOrden,0,0);
			gpGenerarOrden.add(tfNombreOrden,0,1);
			gpGenerarOrden.add(lbNombre,1,0);
			gpGenerarOrden.add(tfEstadoOrden,1,1);
			
		}
		return gpGenerarOrden;
	}	
	public Tab getTabMenuMesero(){
		if(tbMenuMesero==null){
			tbMenuMesero = new Tab("Menu");
			tbMenuMesero.setStyle("-fx-background-color: #0000FF;");
			tbMenuMesero.setContent(this.getContentMenuMesero());
		}
		return tbMenuMesero;
	}
	/**
	*	Este metodo sirve para que los datos del GridPane se pasen a una Tab.
	*	@return tbDeLogin Devuelve la Tab ya con el GridPane del Login. 
	*/
	public Tab getContentMe(){
		if(tbMenu==null){
			tbMenu = new Tab("Menu");
			tbMenu.setStyle("fx-background-color: #0000FF");
			tbMenu.setContent(this.getContentMenu());
		}
		return tbMenu;
	}
	/**
	*	Este metodo sirve para que los datos del GridPane se pasen a una Tab.
	*	@return tbMenuChef Devuelve la Tab ya con el GridPane del Menu principal para Chefs. 
	*/
	public Tab getContentMen(){
		if(tbMenuChef==null){
			tbMenuChef = new Tab("Menu");
			tbMenuChef.setStyle("fx-background-color: #0000FF");
			tbMenuChef.setContent(this.getContentMenuChef());
		}
		return tbMenuChef;
	}
	public GridPane getContentMenuMesero(){
		if(gpMenuMesero==null){
			gpMenuMesero=new GridPane();
			
			imagenTitulo = new ImageView("mesero.jpg");
			GridPane.setMargin(imagenTitulo, new Insets(75,20,0,10));
			btnPlatillo1 = new Button("Platillos");
			btnPlatillo1.setPrefWidth(90);
			btnPlatillo1.addEventHandler(ActionEvent.ACTION, this);
			GridPane.setMargin(btnPlatillo1, new Insets(38,38,0,5));
			btnOrden = new Button("Ordenes");
			btnOrden.setPrefWidth(90);
			btnOrden.addEventHandler(ActionEvent.ACTION, this);
			GridPane.setMargin(btnOrden, new Insets(38,5,0,5));
			btnL2 = new Button("Desconectar");
			btnL2.setPrefWidth(90);
			btnL2.addEventHandler(ActionEvent.ACTION, this);
			btnPedidos = new Button("Pedidos");
			GridPane.setMargin(btnPedidos, new Insets(38,38,0,5));
			btnPedidos.setPrefWidth(90);
			btnPedidos.addEventHandler(ActionEvent.ACTION, this);
			GridPane.setMargin(btnL2, new Insets(38,38,0,5));

			gpMenuMesero.add(imagenTitulo, 0,0,3,6);
		
			gpMenuMesero.add(btnPlatillo1, 5,1);
			gpMenuMesero.add(btnOrden, 5,2);
			gpMenuMesero.add(btnL2, 5, 3);
			gpMenuMesero.add(btnPedidos, 5,4);
		}
		return gpMenuMesero;
	}
	/**
	*	Este metodo sirve para poder poner todas las opciones que puede realizar el usuario en un GridPane.
	*	@return gpMenuChef Devuelve el GridPane lleno de opciones.
	*/
	public GridPane getContentMenuChef(){
		if(gpMenuChef==null){
			gpMenuChef=new GridPane();
			
			imagenTitulo = new ImageView("chef.png");
			GridPane.setMargin(imagenTitulo, new Insets(75,20,0,10));
			btnIngrediente = new Button("Ingredientes");
			btnIngrediente.setPrefWidth(90);
			btnIngrediente.addEventHandler(ActionEvent.ACTION, this);
			GridPane.setMargin(btnIngrediente, new Insets(38,38,0,5));
			btnPlatillo = new Button("Platillos");
			btnPlatillo.setPrefWidth(90);
			btnPlatillo.addEventHandler(ActionEvent.ACTION, this);
			GridPane.setMargin(btnPlatillo, new Insets(38,38,0,5));
			btnPedidos = new Button("Pedidos");
			GridPane.setMargin(btnPedidos, new Insets(76,38,0,5));
			btnPedidos.setPrefWidth(90);
			btnPedidos.addEventHandler(ActionEvent.ACTION, this);
			btnOrden = new Button("Ordenes");
			btnOrden.setPrefWidth(90);
			btnOrden.addEventHandler(ActionEvent.ACTION, this);
			GridPane.setMargin(btnOrden, new Insets(38,5,0,5));
			btnL1 = new Button("Desconectar");
			btnL1.setPrefWidth(90);
			GridPane.setMargin(btnL1, new Insets(38,38,0,5));
			btnL1.addEventHandler(ActionEvent.ACTION, this);

			gpMenuChef.add(imagenTitulo, 0,0,3,6);
			gpMenuChef.add(btnIngrediente, 5,1);
			gpMenuChef.add(btnPlatillo, 5,2);
			gpMenuChef.add(btnOrden, 5,3);
			gpMenuChef.add(btnL1, 5, 4);
			gpMenuChef.add(btnPedidos, 5,0);
		}
		return gpMenuChef;
	}
	/**
	*	Este metodo sirve para poder poner todas las opciones que puede realizar el usuario en un GridPane.
	*	@return gpMenu Devuelve el GridPane lleno de opciones.
	*/
	public GridPane getContentMenu(){
		if(gpMenu==null){
		gpMenu=new GridPane();
		
		imagenTitulo = new ImageView("admin.png");
		GridPane.setMargin(imagenTitulo, new Insets(75,20,0,10));
		btnIngrediente1 = new Button("Ingredientes");
		btnIngrediente1.setPrefWidth(90);
		GridPane.setMargin(btnIngrediente1, new Insets(76,38,0,5));
		btnPlatillo = new Button("Platillos");
		btnPlatillo.setPrefWidth(90);
		GridPane.setMargin(btnPlatillo, new Insets(38,38,0,5));
		btnUsuarios = new Button("Usuarios");
		btnUsuarios.setPrefWidth(90);
		btnL = new Button("Desconectar");
		btnL.setPrefWidth(90);
		GridPane.setMargin(btnL, new Insets(38,38,0,5));
		
		btnPlatillo.addEventHandler(ActionEvent.ACTION, this);
		btnL.addEventHandler(ActionEvent.ACTION, this);
		btnUsuarios.addEventHandler(ActionEvent.ACTION, this);
		btnIngrediente1.addEventHandler(ActionEvent.ACTION, this);
		GridPane.setMargin(btnUsuarios, new Insets(38,5,0,5));

		gpMenu.add(imagenTitulo, 0,0,3,6);
	
		gpMenu.add(btnIngrediente1, 5,1);
		gpMenu.add(btnPlatillo, 5,2);
		gpMenu.add(btnUsuarios, 5,3);
		gpMenu.add(btnL, 5,4);
		}
		return gpMenu;
	}
	/**
	*	Este metodo nos sirve para poder guardar el contenido del tableView en una Tab.
	*	@return tbPlatillo Esta es la tab ya con el contenido del tableViews.
	*/
	public Tab getTabPlatillos(){
		if(tbPlatillo==null){
		tbPlatillo=new Tab("Platillos");
		tbPlatillo.setContent(this.getGridPanePlatillo());
		}
		return tbPlatillo;
	}
	/**
	*	Este metodo sirve para que los datos del GridPane se pasen a una Tab.
	*	@return tbCRUD Devuelve la Tab ya con el GridPane.
	*/
	public Tab getTabCRUD(){
		if(tbCRUD==null){
			tbCRUD = new Tab("Mantenimiento");
			tbCRUD.setContent(getContentCRUD());
		}
	
		return tbCRUD;
	}
	public Tab getTabCRUDIngrediente(){
		if(tbCRUDIngrediente==null){
			tbCRUDIngrediente = new Tab("Mantenimiento");
			tbCRUDIngrediente.setContent(getContentCRUDIngrediente());
		}
	
		return tbCRUDIngrediente;
	}
	/**
	*	Este metodo sirve para que los datos del GridPane se pasen a una Tab.
	*	@return tbCRUDUsuario Devuelve la Tab ya con el GridPane.
	*/
	public Tab getTabCRUDUsuario(){
	if(tbCRUDUsuario==null){
		tbCRUDUsuario = new Tab("Mantenimiento");
		tbCRUDUsuario.setContent(getContentCRUDUSuario());
	}
		return tbCRUDUsuario;
	}
	/**
	*	Este metodo sirve para que se guarde la TableView en una Tab.
	*	@return tbUsuarios devuelve el la tab ya con los valores de la tableview.
	*/
	public Tab getTabUsuarios(){
		if(tbUsuarios==null){
			tbUsuarios=new Tab("Usuarios");
			tbUsuarios.setContent(this.getGridPaneUsuarios());
		}
		return tbUsuarios;
	}
	public GridPane getGridPaneUsuarios(){
		if(gpContenedorUsuario==null){
			gpContenedorUsuario=new GridPane();
			if(hbUsuario==null){
				hbUsuario=new HBox(5);
				hbUsuario.setStyle("-fx-background-color: #F5F5F5");
				btnAgregar = new Button("Agregar");
				btnAgregar.addEventHandler(ActionEvent.ACTION, this);
				btnEliminar = new Button("Eliminar");
				btnEliminar.addEventHandler(ActionEvent.ACTION, this);
				btnL = new Button("Desconectar");
				btnL.addEventHandler(ActionEvent.ACTION, this);
				btnMenuUsu = new Button("Menu");
				btnMenuUsu.addEventHandler(ActionEvent.ACTION, this);
				
				hbUsuario = new HBox();
				hbUsuario.getChildren().add(btnAgregar);
				hbUsuario.getChildren().add(btnEliminar);
				hbUsuario.getChildren().add(btnL);
				hbUsuario.getChildren().add(btnMenuUsu);
				GridPane.setMargin(hbUsuario, new Insets(0,5,0,100));
				gpContenedorUsuario.add(hbUsuario, 0,0,9,1);
			}
			gpContenedorUsuario.add(getContentUsuarios(), 1,1);
		}
		return gpContenedorUsuario;
	}
	/**
	*	Este metodo nos sirve para poder guardar el contenido del tableView en una Tab.
	*	@return tbIngrediente Esta es la tab ya con el contenido del tableViews.
	*/
	public Tab getTabIngrediente(){
		if(tbIngrediente==null){
		tbIngrediente=new Tab("Ingredientes");
		tbIngrediente.setContent(this.getGridPaneIngrediente());
		}
		return tbIngrediente;
	}
	public GridPane getGridPaneIngrediente(){
		if(gpContenedorIngrediente==null){
			gpContenedorIngrediente=new GridPane();
			if(hbIngrediente==null){
				hbIngrediente=new HBox(5);
				hbIngrediente.setStyle("-fx-background-color: #F5F5F5");
				btnAgregaIngrediente = new Button("Agregar");
				btnAgregaIngrediente.addEventHandler(ActionEvent.ACTION, this);
				btnModificarIngrediente = new Button("Modificar");
				btnModificarIngrediente.addEventHandler(ActionEvent.ACTION, this);
				btnEliminarIngrediente=  new Button("Eliminar");
				btnEliminarIngrediente.addEventHandler(ActionEvent.ACTION, this);
				btnL = new Button("Desconectar");
				btnL.addEventHandler(ActionEvent.ACTION, this);
				btnMenuIngrediente  = new Button("Menu");
				btnMenuIngrediente.addEventHandler(ActionEvent.ACTION, this);
				
				hbIngrediente = new HBox();
				hbIngrediente.getChildren().add(btnAgregaIngrediente);
				hbIngrediente.getChildren().add(btnModificarIngrediente);
				hbIngrediente.getChildren().add(btnEliminarIngrediente);
				hbIngrediente.getChildren().add(btnMenuIngrediente);
				hbIngrediente.getChildren().add(btnL);
				GridPane.setMargin(hbIngrediente, new Insets(0,5,0,100));
				gpContenedorIngrediente.add(hbIngrediente, 0,0,9,1);
			}
			gpContenedorIngrediente.add(getContentIngrediente(), 1,1);
		}
		return gpContenedorIngrediente;	
	}
	/**
	*	Este metodo nos sirve para poder hacer las tablas con los datos que ya estan en la Base de Datos de un Ingrediente.
	*	@return tvIngrediente Es el TableView de platillo.
	*/
	public TableView<Ingrediente> getContentIngrediente(){
		if(tvIngrediente==null){
			tvIngrediente = new TableView<Ingrediente>();

			tvIngrediente.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

			TableColumn<Ingrediente, String> columnaNombre = new TableColumn<Ingrediente, String>("NOMBRE DEL INGREDIENTE");
			columnaNombre.setCellValueFactory(new PropertyValueFactory<Ingrediente, String>("nombre"));

			TableColumn<Ingrediente, Integer> columnaPrecio = new TableColumn<Ingrediente, Integer>("PRECIO DEL INGREDIENTE");
			columnaPrecio.setCellValueFactory(new PropertyValueFactory<Ingrediente, Integer>("precio"));

			tvIngrediente.getColumns().setAll(columnaNombre, columnaPrecio);

			tvIngrediente.setItems(mIngrediente.getListaDeIngredientes());
		}
		return tvIngrediente;
	}
	/**
	*	Este metodo sirve para crear un TableView de tipo Usuario en el cual se almacenara todos los datos que ya estan en la base de datos.
	*	@return tvUsuarios Devuelve la TableView ya con los valores insertados.
	*/
	public TableView<Usuario> getContentUsuarios(){
		if(tvUsuarios==null){
			tvUsuarios = new TableView<Usuario>();

			tvUsuarios.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

			TableColumn<Usuario, String> columnaNombre = new TableColumn<Usuario, String>("NOMBRE DEL USUARIO");
			columnaNombre.setCellValueFactory(new PropertyValueFactory<Usuario, String>("nombre"));
			
			TableColumn<Usuario, String> columnaModulo = new TableColumn<Usuario, String>("MODULO DEL USUARIO");
			columnaModulo.setCellValueFactory(new PropertyValueFactory<Usuario, String>("Modulo"));
			
			TableColumn<Usuario, String> columnaNick = new TableColumn<Usuario, String>("NICK DEL USUARIO");
			columnaNick.setCellValueFactory(new PropertyValueFactory<Usuario, String>("nick"));

			TableColumn<Usuario, Integer> columnaEdad = new TableColumn<Usuario, Integer>("EDAD DEL USUARIO");
			columnaEdad.setCellValueFactory(new PropertyValueFactory<Usuario, Integer>("edad"));

			tvUsuarios.getColumns().setAll(columnaNombre, columnaEdad, columnaNick, columnaModulo);

			tvUsuarios.setItems(mUsuario.getListaDeUsuarios());
		}
		return tvUsuarios;
	}
	public GridPane getGridPanePlatillo(){
		if(gpContenedorPlatillo==null){
			gpContenedorPlatillo=new GridPane();
			if(hbPlatillo==null){
				hbPlatillo=new HBox(9);
				hbPlatillo.setStyle("-fx-background-color: #F5F5F5");
				btnA = new Button("Agregar");
				btnA.addEventHandler(ActionEvent.ACTION, this);
				btnD = new Button("Eliminar");
				btnD.addEventHandler(ActionEvent.ACTION, this);
				btnU = new Button("Modificar");
				btnU.addEventHandler(ActionEvent.ACTION, this);
				btnL = new Button("Desconectar");
				btnL.addEventHandler(ActionEvent.ACTION, this);
				btnMenu = new Button("Menu");
				btnMenu.addEventHandler(ActionEvent.ACTION, this);
		
				hbPlatillo = new HBox();
				hbPlatillo.getChildren().add(btnA);
				hbPlatillo.getChildren().add(btnD);
				hbPlatillo.getChildren().add(btnU);
				hbPlatillo.getChildren().add(btnL);
				hbPlatillo.getChildren().add(btnMenu);
				

				GridPane.setMargin(hbPlatillo, new Insets(0,5,0,100));
				gpContenedorPlatillo.add(hbPlatillo, 0,0,21,1);
			}
			gpContenedorPlatillo.add(getContentPlatillos(), 1,1);
		}
		return gpContenedorPlatillo;	
	}
	public GridPane getContentCRUDIngrediente(){
		if(gpContentCRUDIngrediente==null){
			gpContentCRUDIngrediente = new GridPane();
			
			tfNombreIngre = new TextField();
			tfNombreIngre.addEventHandler(KeyEvent.KEY_RELEASED, this);
			tfPrecioIngre = new TextField();
			tfPrecioIngre.addEventHandler(KeyEvent.KEY_RELEASED, this);
			lbNombreA = new Label("Nombre: ");
			lbPrecioA = new Label("Precio: ");
			gpContentCRUDIngrediente.setStyle("-fx-background-image: url(imagenPla.jpg);-fx-background-repeat: no-repeat");

			gpContentCRUDIngrediente.add(lbNombreA, 0,0);
			gpContentCRUDIngrediente.add(lbPrecioA, 0,1);		
		
			gpContentCRUDIngrediente.add(tfNombreIngre, 1,0);
			gpContentCRUDIngrediente.add(tfPrecioIngre, 1,1);
		}
		return gpContentCRUDIngrediente;
	}
	/**
	*	Este metodo sirve para hacer un GridPane que tenga el contenido de un un CRUD de Platillos o ingredientes.
	*	@return gpContentCRUD devuelve el contenido del GridPane del CRUD.
	*/
	public GridPane getContentCRUD(){
		if(gpContentCRUD==null){
			gpContentCRUD = new GridPane();
			
			tfNombreA = new TextField();
			tfNombreA.addEventHandler(KeyEvent.KEY_RELEASED, this);
			tfPrecioA = new TextField();
			tfPrecioA.addEventHandler(KeyEvent.KEY_RELEASED, this);
			lbNombreA = new Label("Nombre: ");
			lbPrecioA = new Label("Precio: ");
			gpContentCRUD.setStyle("-fx-background-image: url(imagenPla.jpg);-fx-background-repeat: no-repeat");

			gpContentCRUD.add(lbNombreA, 0,0);
			gpContentCRUD.add(lbPrecioA, 0,1);		
		
			gpContentCRUD.add(tfNombreA, 1,0);
			gpContentCRUD.add(tfPrecioA, 1,1);
		}
		return gpContentCRUD;
	}
	/**
	*	Este metodo sirve para hacer un GridPane que tenga el contenido de un un CRUD de Usuario.
	*	@return gpContentCRUDUsuario devuelve el contenido del GridPane del Usuario.
	*/
	public GridPane getContentCRUDUSuario(){
		if(gpContentCRUDUsuario==null){
			gpContentCRUDUsuario = new GridPane();
			imagenTitulo = new ImageView("modulos.png");
			tfEdad = new TextField();
			pfPassword = new PasswordField();
			tfNombreU = new TextField();
			tfNick = new TextField();
			tfModulo = new TextField();
			tfNombreModulo = new TextField();
			btnModificarUsuario = new Button("Listo");
			btnModificarUsuario.setPrefWidth(90);
			btnModificarUsuario.addEventHandler(ActionEvent.ACTION, this);
			lbNick = new Label("Nick: ");
			lbEdad = new Label("Edad: ");
			lbPass = new Label("Password: ");
			lbNombreA = new Label("Nombre: ");
			lbModulo = new Label("Escriba el id del Modulo: ");
			lbNombreModulo = new Label("Escriba el Modulo del Usuario");
			
			
			gpContentCRUDUsuario.setStyle("-fx-background-image: url(imagenPla.jpg);-fx-background-repeat: no-repeat");

			gpContentCRUDUsuario.add(lbNick, 0,0);
			gpContentCRUDUsuario.add(lbNombreA, 0,1);		
			gpContentCRUDUsuario.add(lbPass, 0,2);
			gpContentCRUDUsuario.add(lbEdad, 0,3);
			gpContentCRUDUsuario.add(lbModulo, 0,4);
			gpContentCRUDUsuario.add(tfNick, 1,0);
			gpContentCRUDUsuario.add(tfNombreU, 1,1);
			gpContentCRUDUsuario.add(pfPassword, 1,2);
			gpContentCRUDUsuario.add(tfEdad, 1,3);
			gpContentCRUDUsuario.add(tfModulo, 1,4);
			gpContentCRUDUsuario.add(btnModificarUsuario, 2,2);
			gpContentCRUDUsuario.add(imagenTitulo, 5,1,5,6);
			
		}
		return gpContentCRUDUsuario;
	}
	/**
	*	Este metodo nos sirve para poder hacer las tablas con los datos que ya estan en la Base de Datos.
	*	@return tvPlatillos Es el TableView de platillo.
	*/
	public TableView<Platillo> getContentPlatillos(){
	if(tvPlatillos==null){
		tvPlatillos = new TableView<Platillo>();

		tvPlatillos.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		TableColumn<Platillo, String> columnaNombre = new TableColumn<Platillo, String>("NOMBRE DEL PLATILLO");
		columnaNombre.setCellValueFactory(new PropertyValueFactory<Platillo, String>("nombre"));

		TableColumn<Platillo, Integer> columnaPrecio = new TableColumn<Platillo, Integer>("PRECIO DEL PLATILLO");
		columnaPrecio.setCellValueFactory(new PropertyValueFactory<Platillo, Integer>("precio"));
		
		TableColumn<Platillo, Integer> columnaId = new TableColumn<Platillo, Integer>("ID");
		columnaId.setCellValueFactory(new PropertyValueFactory<Platillo, Integer>("idPlatillo"));

		tvPlatillos.getColumns().setAll(columnaNombre, columnaPrecio,columnaId);

		tvPlatillos.setItems(mPlatillo.getListaDePlatillos());
		}
		return tvPlatillos;
	}
	/**
	*	Este metodo sirve para hacer un GridPane que tenga el contenido de un Login
	*	@return gpDeLogin devuelve el contenido del GridPane del login.
	*/
	public GridPane getContentLogin(){
	if(gpDeLogin==null){
			gpDeLogin = new GridPane();
			lbIncorrecto= new Label("INGRESE SUS DATOS PORFAVOR");
			GridPane.setMargin(lbIncorrecto, new Insets(0,100,0,100));
			btLogin = new Button("Login");
			btLogin.setPrefWidth(149);
			btLogin.addEventHandler(ActionEvent.ACTION, this);
			GridPane.setMargin(btLogin, new Insets(0,100,0,100));
			tfNombre = new TextField();
			tfNombre.setPromptText("Usuario");
			tfNombre.addEventHandler(KeyEvent.KEY_RELEASED, this);
			GridPane.setMargin(tfNombre, new Insets(100,100,0,100));
			pfPass = new PasswordField();
			pfPass.setPromptText("Contrasena");
			pfPass.addEventHandler(KeyEvent.KEY_RELEASED, this);
			GridPane.setMargin(pfPass, new Insets(0,100,0,100));
			gpDeLogin.setStyle("fx-background-image: url(comida.jpg);-fx-background-repeat: no-repeat");
			
			gpDeLogin.add(tfNombre, 0, 1);
			gpDeLogin.add(pfPass, 0, 2);
			gpDeLogin.add(btLogin, 0 , 3, 2,1);
			gpDeLogin.add(lbIncorrecto, 0, 4, 2, 1);
		}
		return gpDeLogin;
	}
	/**	
	*	Este metodo nos sirve para instanciar la clase ManejadorUsuario.
	*	@param mUsuario esta variable de tipo ManejadorUsuario es la que se encarga de darle un valor a la instancia.
	*/
	public void setMUsuario(ManejadorUsuario mUsuario){
		this.mUsuario = mUsuario;
	}
	/**	
	*	Este metodo nos sirve para instanciar la clase ManejadorPlatillo.
	*	@param mPlatillo esta variable de tipo ManejadorPlatillo es la que se encarga de darle un valor a la instancia.
	*/
	public void setMPlatillo(ManejadorPlatillo mPlatillo){
		this.mPlatillo = mPlatillo;
	}
	public void setMOrden(ManejadorOrden mOrden){
		this.mOrden = mOrden;
	}
	public void setMPedido(ManejadorPedido mPedido){
		this.mPedido=mPedido;
	}
	public void setMDetallePedido(ManejadorDetallePedido mDetallePedido){
		this.mDetallePedido=mDetallePedido;
	}
	/**	
	*	Este metodo nos sirve para instanciar la clase ManejadorIngrediente.
	*	@param mIngrediente esta variable de tipo ManejadorIngrediente es la que se encarga de darle un valor a la instancia.
	*/
	public void setMIngrediente(ManejadorIngrediente mIngrediente){
		this.mIngrediente=mIngrediente;
	}
	/**
	*	Este metodo nos sirve para identificar que platillo se va a modificar.
	* 	@param platillo esta variable de tipo Platillo es el platillo que se va a modificar.
	*/
	public void setPlatillo(Platillo platillo){
		tfNombreA.setText(platillo.getNombre());
		tfPrecioA.setText(String.valueOf(platillo.getPrecio()));
	}
	public void setUsuario(Usuario usuario){
		tfNick.setText(usuario.getNick());
		tfNombreU.setText(usuario.getNombre());
		pfPassword.setText(usuario.getPass());
		tfEdad.setText(Integer.toString(usuario.getEdad()));
		tfModulo.setText(Integer.toString(usuario.getIdModulo()));
	}
	public void setIngrediente(Ingrediente ingre){
		tfNombreIngre.setText(ingre.getNombre());
		tfPrecioIngre.setText(String.valueOf(ingre.getPrecio()));
	}
	public void setOrden(Orden orden){
		tfNombreOrden.setText(orden.getNombre());
		tfEstadoOrden.setText(Integer.toString(orden.getIdEstado()));
	}
	public void setDetallePedido(DetallePedido ped){
		tfIdPedido.setText(Integer.toString(ped.getIdPedido()));
		tfPlatillo.setText(Integer.toString(ped.getIdPlatillo()));
		tfCantidad.setText(Integer.toString(ped.getCantidad()));
	}
	public void setPedido(Pedido ped){
		tfNombrePedido.setText(ped.getNombre());
		tfIdOrden.setText(Integer.toString(ped.getIdOrden()));
	}
	/**
	*	Este metodo nos sirve para verificar que lo que se esta escribiendo en el textfield o passwordfield no es un dato nulo.
	*	@return true or false depende de si esta vacio o no.
	*/
	public boolean validarDatos(){
		return !tfNombreA.getText().trim().equals("") && !tfPrecioA.getText().trim().equals("");
	}
	public boolean validarDatosUsuario(){
		return !tfNick.getText().trim().equals("") && !tfNombreU.getText().trim().equals("") && !pfPassword.getText().trim().equals("") && !tfEdad.getText().trim().equals("") && !tfModulo.getText().trim().equals("") && !tfNombreModulo.getText().trim().equals("");
	}
	public boolean validarDatosIngre(){
		return !tfNombreIngre.getText().trim().equals("") && !tfPrecioIngre.getText().trim().equals("");
	}
	public boolean validarDatosOrden(){
		return !tfNombreOrden.getText().trim().equals("") && !tfEstadoOrden.getText().trim().equals("");
	}
	public boolean validarDatosPedido(){
		return !tfNombrePedido.getText().trim().equals("") && !tfIdOrden.getText().trim().equals("");
	}
	public boolean validarDatosPedidoPlatillo(){
		return !tfIdPedido.getText().trim().equals("") && !tfCantidad.getText().trim().equals("");
	}
	/**
	*	Este metodo es el que recive todos los eventos o acciones que puede realizar el programa.
	*	@param event esta variable de tipo Event recive el evento que puede hacer cada cosa.
	*/
	public void handle(Event event){
		if(event instanceof KeyEvent){
			KeyEvent keyEvent = (KeyEvent)event;
			if(keyEvent.getCode()==KeyCode.ENTER){
				if(!tfNombre.getText().trim().equals("") & !pfPass.getText().trim().equals("")){
					if(mUsuario.conectar(tfNombre.getText(), pfPass.getText())){
						Usuario user=mUsuario.buscarUsuario(tfNombre.getText(), pfPass.getText());
						tfNombre.setText("");
						pfPass.setText("");
						switch(user.getIdModulo()){
							case 1:
								getTabPane().getTabs().clear();
								getTabPane().getTabs().add(getContentMen());
								getTabPane().getTabs().remove(getTabContent());
							break;
							case 2:
								getTabPane().getTabs().clear();
								getTabPane().getTabs().add(getTabMenuMesero());
								getTabPane().getTabs().remove(getTabContent());
							break;
							case 3:
								getTabPane().getTabs().clear();
								getTabPane().getTabs().add(getContentMe());
								getTabPane().getTabs().remove(getTabContent());
							break;
						};
					}else{
						lbIncorrecto = new Label("Usuario o contrasena incorrectos");
					}
				}
				if(event.getSource().equals(tfNombreA)  || event.getSource().equals(tfPrecioA)){
					if(validarDatos()){
						Platillo platillo = new Platillo(0, Integer.parseInt(tfPrecioA.getText()), tfNombreA.getText());
						if(estadoMantenimiento){
							platillo.setIdPlatillo(platilloModificar.getIdPlatillo());
							mPlatillo.modificarPlatillo(platillo);
						}else{
							mPlatillo.agregarPlatillo(platillo);
						}
						getTabPane().getTabs().remove(getTabCRUD());
					}
				}
				if(event.getSource().equals(tfPrecioIngre) || event.getSource().equals(tfNombreIngre)){
					if(validarDatosIngre()){
						Ingrediente ingre= new Ingrediente(0, Integer.parseInt(tfPrecioIngre.getText()),tfNombreIngre.getText());
						if(estadoMantenimientoIngre){
							ingre.setIdIngrediente(ingredienteModificar.getIdIngrediente());
							mIngrediente.modificarIngrediente(ingre);
						}else{
							mIngrediente.agregarIngrediente(ingre);
						}
						getTabPane().getTabs().remove(getTabCRUDIngrediente());
					}
				}else if(event.getSource().equals(tfNombreOrden) || event.getSource().equals(tfEstadoOrden)){
					if(validarDatosOrden()){
						Orden or=new Orden(0, tfNombreOrden.getText(), Integer.parseInt(tfEstadoOrden.getText()),0);
						if(estadoMantenimientoOrden){
							or.setIdOrden(ordenModificar.getIdOrden());
							mOrden.modificarOrden(or);
						}else{
							mOrden.agregarOrden(or);
						}
						getTabPane().getTabs().remove(getTabGenerarOrden());
					}
				}else if(event.getSource().equals(tfIdOrden)|| event.getSource().equals(tfNombrePedido)){
					if(validarDatosPedido()	){
						Pedido ped=new Pedido(0, tfNombrePedido.getText(),0 ,Integer.parseInt(tfIdOrden.getText()));
						mPedido.agregarPedido(ped);
					}
					getTabPane().getTabs().remove(getTabCrearPedido());
				}else if(event.getSource().equals(tfIdPedido) || event.getSource().equals(tfCantidad) || event.getSource().equals(tfPlatillo)){
					if(validarDatosPedidoPlatillo()){
						DetallePedido ped= new DetallePedido(Integer.parseInt(tfIdPedido.getText()), Integer.parseInt(tfPlatillo.getText()), Integer.parseInt(tfCantidad.getText()));
						mDetallePedido.agregarOrden(ped);
					}
					getTabPane().getTabs().remove(getTabPedidoPlatillo());
				}
			}
		}else if(event instanceof ActionEvent){
		
			if(event.getSource().equals(btnD)){
			
				ObservableList<Platillo> Platillos = getContentPlatillos().getSelectionModel().getSelectedItems();
				ArrayList<Platillo> listaNoObservable = new ArrayList<Platillo>(Platillos);
				
				for(Platillo Platillo : listaNoObservable){
					mPlatillo.eliminarPlatillo(Platillo);	
				}
			}else if(event.getSource().equals(btnModificarUsuario)){
				Usuario usuario = new Usuario(0, Integer.parseInt(tfModulo.getText()),tfNombreU.getText(),pfPassword.getText(),Integer.parseInt(tfEdad.getText()),tfNick.getText(),"Modulo");
				mUsuario.agregarUsuario(usuario);
				getTabPane().getTabs().remove(getTabCRUDUsuario());	
			}else if(event.getSource().equals(btnEliminar)){
			
				ObservableList<Usuario> Usuarios = getContentUsuarios().getSelectionModel().getSelectedItems();
				ArrayList<Usuario> listaNoObservable = new ArrayList<Usuario>(Usuarios);
				
				for(Usuario user : listaNoObservable){
					mUsuario.eliminarUsuario(user);	
				}
			}else if(event.getSource().equals(btnL)){
			
				getTabPane().getTabs().clear();
				getTabPane().getTabs().add(getTabContent());
				
			}else if(event.getSource().equals(btnL1)){
			
				getTabPane().getTabs().clear();
				getTabPane().getTabs().add(getTabContent());
				
			}else if(event.getSource().equals(btnA)){
			
				estadoMantenimiento = false;
				if(!getTabPane().getTabs().contains(getTabCRUD()))
					getTabPane().getTabs().add(getTabCRUD());
				getTabPane().getSelectionModel().select(getTabCRUD());
				setPlatillo(new Platillo());
				
			}
			else if(event.getSource().equals(btnU)){
			
				estadoMantenimiento = true;
				if(!getTabPane().getTabs().contains(getTabCRUD()))
					getTabPane().getTabs().add(getTabCRUD());
				getTabPane().getSelectionModel().select(getTabCRUD());
				platilloModificar = getContentPlatillos().getSelectionModel().getSelectedItem();
				setPlatillo(platilloModificar);
				
			}else if(event.getSource().equals(btnPlatillo)){
			
				getTabPane().getTabs().clear();
				getTabPane().getTabs().add(getTabPlatillos());
				
			}
			else if(event.getSource().equals(btLogin)){
				if(!tfNombre.getText().trim().equals("") & !pfPass.getText().trim().equals("")){
					if(mUsuario.conectar(tfNombre.getText(), pfPass.getText())){
						Usuario user=mUsuario.buscarUsuario(tfNombre.getText(), pfPass.getText());
						tfNombre.setText("");
						pfPass.setText("");
						
						switch(user.getIdUsuario()){
							case 1:
								getTabPane().getTabs().clear();
								getTabPane().getTabs().add(getContentMen());
								getTabPane().getTabs().remove(getTabContent());
							break;
							case 2:
								getTabPane().getTabs().clear();
								getTabPane().getTabs().add(getTabMenuMesero());
								getTabPane().getTabs().remove(getTabContent());
							break;
							case 3:
								getTabPane().getTabs().clear();
								getTabPane().getTabs().add(getContentMe());
								getTabPane().getTabs().remove(getTabContent());
							break;
						}
						
					}else{
						lbIncorrecto = new Label("Usuario o contrasena incorrectos");
					}
				}
			}
			else if(event.getSource().equals(btnUsuarios)){
			
				getTabPane().getTabs().clear();
				getTabPane().getTabs().add(getTabUsuarios());
				
			}
			else if(event.getSource().equals(btnIngrediente)){
			
				getTabPane().getTabs().clear();
				getTabPane().getTabs().add(getTabIngrediente());
			
			}else if(event.getSource().equals(btnIngrediente1)){
				
				getTabPane().getTabs().clear();
				getTabPane().getTabs().add(getTabIngrediente());
			}
			else if(event.getSource().equals(btnMenuUsu)){
			
				getTabPane().getTabs().clear();
				getTabPane().getTabs().add(getContentMe());
				getTabPane().getTabs().remove(getTabContent());	
			}else if (event.getSource().equals(btnAgregar)){
			
				estadoMantenimientoUsurio = false;
				if(!getTabPane().getTabs().contains(getTabCRUDUsuario()))
					getTabPane().getTabs().add(getTabCRUDUsuario());
				getTabPane().getSelectionModel().select(getTabCRUDUsuario());
				setUsuario(new Usuario());
				
			}
			else if(event.getSource().equals(btnMenu)){
				Usuario user=mUsuario.getAutenticado();
				switch(user.getIdModulo()){
					case 1:
						getTabPane().getTabs().clear();
						getTabPane().getTabs().add(getContentMen());
						getTabPane().getTabs().remove(getTabContent());
					break;
					case 2:
						getTabPane().getTabs().clear();
						getTabPane().getTabs().add(getTabMenuMesero());
						getTabPane().getTabs().remove(getTabContent());
					break;
					case 3:
						getTabPane().getTabs().clear();
						getTabPane().getTabs().add(getContentMe());
						getTabPane().getTabs().remove(getTabContent());
					break;
				}
			}
			else if(event.getSource().equals(btnAgregaIngrediente)){
				estadoMantenimientoIngre = false;
				if(!getTabPane().getTabs().contains(getTabCRUDIngrediente()))
					getTabPane().getTabs().add(getTabCRUDIngrediente());
				getTabPane().getSelectionModel().select(getTabCRUDIngrediente());
				setIngrediente(new Ingrediente());
			}else if(event.getSource().equals(btnModificarIngrediente)){
				estadoMantenimientoIngre = true;
				if(!getTabPane().getTabs().contains(getTabCRUDIngrediente()))
					getTabPane().getTabs().add(getTabCRUDIngrediente());
				getTabPane().getSelectionModel().select(getTabCRUDIngrediente());
				ingredienteModificar = getContentIngrediente().getSelectionModel().getSelectedItem();
				setIngrediente(ingredienteModificar);
			}else if(event.getSource().equals(btnOrden)){
				getTabPane().getTabs().clear();
				getTabPane().getTabs().add(getTabOrden());
			}else if(event.getSource().equals(btnDesconectarOrden)){
				getTabPane().getTabs().clear();
				getTabPane().getTabs().add(getTabContent());
			}else if(event.getSource().equals(btnMenuOrden)){
				Usuario user=mUsuario.getAutenticado();
				switch(user.getIdModulo()){
					case 1:
						getTabPane().getTabs().clear();
						getTabPane().getTabs().add(getContentMen());
						getTabPane().getTabs().remove(getTabContent());
					break;
					case 2:
						getTabPane().getTabs().clear();
						getTabPane().getTabs().add(getTabMenuMesero());
						getTabPane().getTabs().remove(getTabContent());
					break;
					case 3:
						getTabPane().getTabs().clear();
						getTabPane().getTabs().add(getContentMe());
						getTabPane().getTabs().remove(getTabContent());
					break;
				}
			}else if(event.getSource().equals(btnGenerarOrden)){
				estadoMantenimientoOrden = false;
				if(!getTabPane().getTabs().contains(getTabGenerarOrden()))
					getTabPane().getTabs().add(getTabGenerarOrden());
				getTabPane().getSelectionModel().select(getTabGenerarOrden());
				setOrden(new Orden());
			}else if(event.getSource().equals(btnCambiarEstado)){
				estadoMantenimientoOrden = true;
				if(!getTabPane().getTabs().contains(getTabGenerarOrden()))
					getTabPane().getTabs().add(getTabGenerarOrden());
				getTabPane().getSelectionModel().select(getTabGenerarOrden());
				ordenModificar = getTableViewOrden().getSelectionModel().getSelectedItem();
				setOrden(ordenModificar);
			}else if(event.getSource().equals(btnPedidos)){
				getTabPane().getTabs().clear();
				getTabPane().getTabs().add(getTabPedido());
			}else if(event.getSource().equals(btnCrearPedido)){
				if(!getTabPane().getTabs().contains(getTabCrearPedido()))
					getTabPane().getTabs().add(getTabCrearPedido());
				getTabPane().getSelectionModel().select(getTabCrearPedido());
				setPedido(new Pedido());
			}else if(event.getSource().equals(btnAgregarIngredientePlatillo)){
				if(!getTabPane().getTabs().contains(getTabPedidoPlatillo()))
					getTabPane().getTabs().add(getTabPedidoPlatillo());
				getTabPane().getSelectionModel().select(getTabPedidoPlatillo());
				setDetallePedido(new DetallePedido());
			}else if(event.getSource().equals(btnEliminarIngrediente)){
				ObservableList<Ingrediente> Ingrediente = getContentIngrediente().getSelectionModel().getSelectedItems();
				ArrayList<Ingrediente> listaNoObservable = new ArrayList<Ingrediente>(Ingrediente);
				
				for(Ingrediente ingre : listaNoObservable){
					mIngrediente.eliminarIngrediente(ingre);	
				}
			}else if(event.getSource().equals(btnMenuIngrediente)){
				Usuario user=mUsuario.getAutenticado();
				switch(user.getIdModulo()){
					case 1:
						getTabPane().getTabs().clear();
						getTabPane().getTabs().add(getContentMen());
						getTabPane().getTabs().remove(getTabContent());
					break;
					case 2:
						getTabPane().getTabs().clear();
						getTabPane().getTabs().add(getTabMenuMesero());
						getTabPane().getTabs().remove(getTabContent());
					break;
					case 3:
						getTabPane().getTabs().clear();
						getTabPane().getTabs().add(getContentMe());
						getTabPane().getTabs().remove(getTabContent());
					break;
				}
			}
			else if(event.getSource().equals(btnDeslogearse)){
				getTabPane().getTabs().clear();
				getTabPane().getTabs().add(getTabContent());
			}else if(event.getSource().equals(btnRegresarMenu)){
				Usuario user=mUsuario.getAutenticado();
				switch(user.getIdModulo()){
					case 1:
						getTabPane().getTabs().clear();
						getTabPane().getTabs().add(getContentMen());
						getTabPane().getTabs().remove(getTabContent());
					break;
					case 2:
						getTabPane().getTabs().clear();
						getTabPane().getTabs().add(getTabMenuMesero());
						getTabPane().getTabs().remove(getTabContent());
					break;
					case 3:
						getTabPane().getTabs().clear();
						getTabPane().getTabs().add(getContentMe());
						getTabPane().getTabs().remove(getTabContent());
					break;
				}
			}else if(event.getSource().equals(btnListoPedido)){
				DetallePedido Detalle = new DetallePedido(Integer.parseInt(tfIdPedido.getText()), Integer.parseInt(tfPlatillo.getText()), Integer.parseInt(tfCantidad.getText()));
				setDetallePedido(new DetallePedido());
				mDetallePedido.agregarOrden(Detalle);
			}else if(event.getSource().equals(btnVerPedidos)){
				if(!getTabPane().getTabs().contains(getTabDetallePedido())){
					getTabPane().getTabs().add(getTabDetallePedido());
				}
			}else if(event.getSource().equals(btnPlatillo1)){
				getTabPane().getTabs().clear();
				getTabPane().getTabs().add(getTabPlatillos());
			}else if(event.getSource().equals(btnOrden)){
				getTabPane().getTabs().clear();
				getTabPane().getTabs().add(getTabOrden());
			}else if(event.getSource().equals(btnL2)){
				getTabPane().getTabs().clear();
				getTabPane().getTabs().add(getTabContent());
			}else if(event.getSource().equals(btnFacturarOrdenes)){
				Stage dialog = new Stage();
				dialog.setScene(new Scene(new Label("AHORITA SE IMPRIMIRA LA FACTURA DE LA ORDEN SELECCIONADA")));
				dialog.initOwner(primaryStage);
				dialog.initModality(Modality.WINDOW_MODAL);
				dialog.show();
			}
		}
	}
}