/**
*	Esta clase es el manejador de Usuarios.
*	@author Hermes Sotoj
*/
package org.hermes.manejadores;

import org.hermes.beans.Usuario;
import org.hermes.db.Conexion;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.SQLException;
import java.sql.ResultSet;

public class ManejadorUsuario{
	private ObservableList<Usuario> listaDeUsuarios;
	private Usuario usuarioConectado;
	private Conexion cnx;
	/**
	*	Este es el constructor de la clase ManejadorUsuario
	*	@param conexion Esta variable es la encargada de establecer conexion con la base de datos.
	*/
	public ManejadorUsuario(Conexion conexion){
		listaDeUsuarios = FXCollections.observableArrayList();
		cnx = conexion;
		this.actualizarLista();
	}
	/**
	*	Este metodo nos sirve para mantener actualizarLista nuestra lista de usuarios con los de la base de datos.
	*/
	public void actualizarLista(){
		listaDeUsuarios.clear();
		ResultSet resultado = cnx.ejecutarConsulta("SELECT Usuario.idUsuario,Usuario.nombre, Usuario.nick ,Usuario.pass, Usuario.edad , Modulo.nombre AS Modulo, Usuario.idModulo FROM Usuario INNER JOIN Modulo ON Usuario.idModulo=Modulo.idModulo");
		try{
			if(resultado!=null){
				while(resultado.next()){
					Usuario usuario = new Usuario(resultado.getInt("idUsuario"), resultado.getInt("idModulo"), resultado.getString("nombre"), resultado.getString("pass"), resultado.getInt("edad"),resultado.getString("nick"),resultado.getString("Modulo"));
					listaDeUsuarios.add(usuario);
				}
			}
		}catch(SQLException sql){
			sql.printStackTrace();
		}
	}
	/**
	*	Este metodo nos devuelve la lista de usuarios.
	*	@return listaDeUsuarios Devuelve la lista de usuarios.
	*/
	public ObservableList<Usuario> getListaDeUsuarios(){
		actualizarLista();
		return this.listaDeUsuarios;
	}
	/**
	*	Este metodo nos sirve para desconectar a un usuario que este conectado.
	*/
	public void desconectar(){
		this.usuarioConectado=null;
	}
	/**
	*	Este metodo nos sirve para poder autenticar un usuario y conectarlo.
	*	@param nombre Es el nick del usuario que se quiere autenticar.
	*	@param pass Es la contraseña del usuario que se quiere autenticar.
	*	@return boolean Devuelve true si la busqueda es efectiva y false si no.
	*/
	public boolean conectar(String nombre, String pass){
		ResultSet resultado = cnx.ejecutarConsulta("SELECT Usuario.idUsuario,Usuario.nombre, Usuario.nick ,Usuario.pass, Usuario.edad , Modulo.nombre AS Modulo, Usuario.idModulo FROM Usuario INNER JOIN Modulo ON Usuario.idModulo=Modulo.idModulo WHERE Usuario.nick='"+nombre+"' AND pass='"+pass+"'");
		try{
			if(resultado!=null){
				if(resultado.next()){
					usuarioConectado = new Usuario(resultado.getInt("idUsuario"), resultado.getInt("idModulo"), resultado.getString("nombre"), resultado.getString("pass"), resultado.getInt("edad"),resultado.getString("nick"),resultado.getString("Modulo"));
					return true;
				}
			}
		}catch(SQLException sql){
			sql.printStackTrace();
		}
		return false;
	}
	/**
	*	Este metodo nos sirve para poder autenticar un usuario y conectarlo.
	*	@param nombre Es el nick del usuario que se quiere autenticar.
	*	@param pass Es la contraseña del usuario que se quiere autenticar.
	*	@return usuarioConectado Devuelve el usuario si la busqueda es exitosa si no retorna null
	*/
	public Usuario buscarUsuario(String nombre, String pass){
		ResultSet resultado = cnx.ejecutarConsulta("SELECT Usuario.idUsuario,Usuario.nombre, Usuario.nick ,Usuario.pass, Usuario.edad , Modulo.nombre AS Modulo, Usuario.idModulo FROM Usuario INNER JOIN Modulo ON Usuario.idModulo=Modulo.idModulo WHERE Usuario.nick='"+nombre+"' AND pass='"+pass+"'");
		try{
			if(resultado!=null){
				if(resultado.next()){
					usuarioConectado = new Usuario(resultado.getInt("idUsuario"), resultado.getInt("idModulo"), resultado.getString("nombre"), resultado.getString("pass"), resultado.getInt("edad"),resultado.getString("nick"),resultado.getString("Modulo"));
					return usuarioConectado;
				}
			}
		}catch(SQLException sql){
			sql.printStackTrace();
		}
		return null;
	}
	public Usuario getAutenticado(){
		return usuarioConectado;
	}
	/**
	*	Este metodo nos sirve para poder eliminar un usuario.
	*	@param usuario Es el usuario que se va a eliminar
	*/
	public void eliminarUsuario(Usuario usuario){
		cnx.ejecutarSentencia("DELETE FROM Usuario WHERE idUsuario="+usuario.getIdUsuario());
		actualizarLista();
	}
	/**
	*	Este metodo nos sirve para poder agregar un usuario.
	*	@param usuario Es el usuario que se va a agregar
	*/
	public void agregarUsuario(Usuario usuario){
		cnx.ejecutarSentencia("INSERT INTO Usuario(nombre,edad,nick,pass,idModulo) VALUES ('"+usuario.getNombre()+"',"+usuario.getEdad()+",'"+usuario.getNick()+"','"+usuario.getPass()+"',"+usuario.getIdModulo()+")");
		actualizarLista();
	}

}
