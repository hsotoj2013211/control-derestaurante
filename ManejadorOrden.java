/**
*
*/
package org.hermes.manejadores;

import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import org.hermes.db.Conexion;
import org.hermes.beans.Orden;

public class ManejadorOrden{
	private  Conexion cnx;
	private  ObservableList<Orden> listaDeOrdenes;
	/**
	*	Este es el constructor de la clase ManejadorIngrediente se ejecuta cada vez que se instancia la clase.
	*	@param cnx Esta variable es la que se encarga de recivir la conexion a la base de datos.
	*/
	public ManejadorOrden(Conexion cnx){
		this.cnx=cnx;
		this.listaDeOrdenes=FXCollections.observableArrayList();
	}
	/**
	*	Este metodo nos sirve para poder actualizar la lista cada vez que se hace un cambio en la base de datos.
	*/
	public void actualizarListaDeOrdenes(){
		listaDeOrdenes.clear();
		ResultSet resultado = cnx.ejecutarConsulta("SELECT idOrden, nombre, idEstado,gastoTotal FROM Orden");
		try{
			while(resultado.next()){
				Orden orden = new Orden(resultado.getInt("idOrden"), resultado.getString("nombre"), resultado.getInt("idEstado"), resultado.getInt("gastoTotal"));
				listaDeOrdenes.add(orden);
			}
		}catch(SQLException sql){;
			sql.printStackTrace();
		}
	}
	/**
	*	Este metodo nos sirve para poder actuarlizar la lista de ingredientes con los datos de la base de datos.
	*	@return listaDeOrdenes Devuelve la lista de ingredientes ya actualizadas.
	*/
	public ObservableList<Orden> getListaOrdenes(){
		actualizarListaDeOrdenes();
		return listaDeOrdenes;
	}
	/**
	*	Este metodo sirve para eliminar un ingrediente.
	*	@param ingrediente es el ingrediente que en este caso se va a eliminar
	*/
	public void eliminarOrden(Orden orden){
		cnx.ejecutarSentencia("DELETE FROM Orden WHERE idOrden="+orden.getIdOrden());
		actualizarListaDeOrdenes();
	}
	/**
	*	Este metodo sirve para agregar un ingrediente.
	*	@param ingrediente es el ingrediente que en este caso se va a agregar
	*/
	public void agregarOrden(Orden orden){
		cnx.ejecutarSentencia("INSERT INTO Orden(nombre,idEstado,gastoTotal) VALUES ('"+orden.getNombre()+"',"+1+","+orden.getTotal()+")");
		actualizarListaDeOrdenes();
	}
	/**
	*	Este metodo sirve para modificar un ingrediente.
	*	@param ingrediente es el ingrediente que en este caso se va a agregar
	*/
	public void modificarOrden(Orden orden){
		cnx.ejecutarSentencia("UPDATE Orden SET nombre='"+orden.getNombre()+"', idEstado="+orden.getIdEstado()+" WHERE idOrden="+orden.getIdOrden());
		actualizarListaDeOrdenes();
	}
}

