/**
*
*/
package org.hermes.manejadores;

import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import org.hermes.db.Conexion;
import org.hermes.beans.Ingrediente;

public class ManejadorIngrediente{
	private Conexion cnx;
	private ObservableList<Ingrediente> listaDeIngredientes;
	/**
	*	Este es el constructor de la clase ManejadorIngrediente se ejecuta cada vez que se instancia la clase.
	*	@param cnx Esta variable es la que se encarga de recivir la conexion a la base de datos.
	*/
	public ManejadorIngrediente(Conexion cnx){
		this.cnx=cnx;
		this.listaDeIngredientes=FXCollections.observableArrayList();
	}
	/**
	*	Este metodo nos sirve para poder actualizar la lista cada vez que se hace un cambio en la base de datos.
	*/
	public void actualizarListaDeIngredientes(){
		listaDeIngredientes.clear();
		ResultSet resultado = cnx.ejecutarConsulta("SELECT idIngrediente, precio, nombre FROM Ingrediente");
		try{
			while(resultado.next()){
				Ingrediente Ingrediente = new Ingrediente(resultado.getInt("idIngrediente"), resultado.getInt("precio"), resultado.getString("nombre"));
				listaDeIngredientes.add(Ingrediente);
			}
		}catch(SQLException sql){
			sql.printStackTrace();
		}
	}
	/**
	*	Este metodo nos sirve para poder actuarlizar la lista de ingredientes con los datos de la base de datos.
	*	@return listaDeIngredientes Devuelve la lista de ingredientes ya actualizadas.
	*/
	public ObservableList<Ingrediente> getListaDeIngredientes(){
		actualizarListaDeIngredientes();
		return listaDeIngredientes;
	}
	/**
	*	Este metodo sirve para eliminar un ingrediente.
	*	@param ingrediente es el ingrediente que en este caso se va a eliminar
	*/
	public void eliminarIngrediente(Ingrediente ingrediente){
		cnx.ejecutarSentencia("DELETE FROM Ingrediente WHERE idIngrediente="+ingrediente.getIdIngrediente());
		actualizarListaDeIngredientes();
	}
	/**
	*	Este metodo sirve para agregar un ingrediente.
	*	@param ingrediente es el ingrediente que en este caso se va a agregar
	*/
	public void agregarIngrediente(Ingrediente ingrediente){
		cnx.ejecutarSentencia("INSERT INTO Ingrediente(nombre,precio) VALUES ('"+ingrediente.getNombre()+"',"+ingrediente.getPrecio()+")");
		actualizarListaDeIngredientes();
	}
	/**
	*	Este metodo sirve para modificar un ingrediente.
	*	@param ingrediente es el ingrediente que en este caso se va a agregar
	*/
	public void modificarIngrediente(Ingrediente ingrediente){
		cnx.ejecutarSentencia("UPDATE Ingrediente SET nombre='"+ingrediente.getNombre()+"', precio="+ingrediente.getPrecio()+" WHERE idIngrediente="+ingrediente.getIdIngrediente());
		actualizarListaDeIngredientes();
	}
}

