/**
* Esta clase es la que contiene la conexion y la que establece conexion con la base de datos.
* @author Hermes Sotoj
*/

package org.hermes.db;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.Statement;

public class Conexion{
	private Connection cnx;
	private Statement stm;
	/**
	*	Este metodo es el constructor se ejecuta cada vez que se instancia la clase Conexion.
	*/
	public Conexion(){
		try{		
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			cnx = DriverManager.getConnection("jdbc:sqlserver://localhost:50169;databaseName=dbRestaurante2013211", "Hermes", "123");
			stm = cnx.createStatement();
		}catch(ClassNotFoundException cl){
			cl.printStackTrace();
		}catch(SQLException sql){
			sql.printStackTrace();
		}
	}
	/**
	*	Este metodo es el que ejecuta las sentencias que se quieren realizar en la base de datos.
	*	@return boolean Depende si se ejecuto la sentencia correctamente devolvera true o false.
	*/
	public boolean ejecutarSentencia(String sentencia){
		boolean resultado = false;
		try{
			resultado = stm.execute(sentencia);
		}catch(SQLException sql){
			sql.printStackTrace();
		}
		return resultado;
	}
	/**
	*	Este metodo es el que ejecuta las consultas en la base de datos.
	*	@return resultado Devuelve el valor de la consulta.
	*/
	public ResultSet ejecutarConsulta(String consulta){
		ResultSet resultado = null;
		try{
			resultado = stm.executeQuery(consulta);
		}catch(SQLException sql){
			sql.printStackTrace();
		}
		return resultado;
	}
}
